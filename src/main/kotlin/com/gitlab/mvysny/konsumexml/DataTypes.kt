@file:Suppress("FunctionName")

package com.gitlab.mvysny.konsumexml

import java.lang.IllegalStateException
import java.math.BigDecimal
import java.util.*
import javax.xml.bind.DatatypeConverter
import kotlin.math.absoluteValue

/**
 * Utility method for expecting an element with given [name] and text contents only; the trimmed text contents are
 * returned and converted to integer. If the conversion throws, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown.
 * @throws KonsumerException if the element is absent, or the conversion fails or if the number is outside of given range.
 */
@KonsumerDsl
public fun Konsumer.childInt(name: String,
                      range: IntRange = Int.MIN_VALUE..Int.MAX_VALUE): Int =
        childText(name) { it.toInt(range) }

/**
 * Utility method for expecting an element with given [name] and text contents only; the trimmed text contents are
 * returned and converted to integer. If the conversion throws, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown.
 *
 * The function does nothing and returns `null` if there is no such next element.
 * @throws KonsumerException if the element is present, and either the conversion fails, or the number is outside of given range.
 */
@KonsumerDsl
public fun Konsumer.childIntOpt(name: String,
                         range: IntRange = Int.MIN_VALUE..Int.MAX_VALUE): Int? =
        childTextOrNull(name) { it.toInt(range) }

/**
 * Utility method for expecting a zero-to-infinite elements with given [name] and text contents only; the trimmed text contents are
 * converted to integer. If the conversion throws, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown.
 * @throws KonsumerException if the conversion fails or if the number is outside of given range.
 */
@KonsumerDsl
public fun Konsumer.childrenInt(name: String,
                         minCount: Int = 0,
                         maxCount: Int = Int.MAX_VALUE,
                         range: IntRange = Int.MIN_VALUE..Int.MAX_VALUE): List<Int> =
        childrenText(name, minCount, maxCount) { it.toInt(range) }


/**
 * Retrieves an int value of attribute with given [localName]. If the conversion throws, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown. Fails if the attribute is missing or is outside of given [range].
 * @throws KonsumerException if the attribute is missing, conversion fails or the value is out of range.
 */
@KonsumerDsl
public fun AttributeKonsumer.getValueInt(localName: String,
                                  range: IntRange = Int.MIN_VALUE..Int.MAX_VALUE): Int =
        getValue(localName) { it.toInt(range) }

/**
 * Retrieves an int value of attribute with given [localName]. If the conversion throws, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown. Returns `null` if the attribute is missing. Fails if the attribute is present
 * and is outside of given range.
 * @throws KonsumerException if the conversion fails or the value is out of range.
 */
@KonsumerDsl
public fun AttributeKonsumer.getValueIntOrNull(localName: String,
                                               range: IntRange = Int.MIN_VALUE..Int.MAX_VALUE): Int? =
        getValueOrNull(localName) { it.toInt(range) }

/**
 * Retrieves an enum value of attribute with given [localName]. If there is no such enum constant, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown. Returns `null` if the attribute is missing. Fails if the attribute is present
 * and is outside of given range.
 * @param xmlValue extracts expected XML value from an enum constant. The XML is expected to contain one of values returned by this function.
 * Defaults to [Enum.name].
 * @throws KonsumerException if the conversion fails or the value is out of range.
 */
@KonsumerDsl
public inline fun <reified E : Enum<E>> AttributeKonsumer.getValueEnumOrNull(
    localName: String,
    crossinline xmlValue: (E) -> String = { it.name }
): E? =
    getValueOrNull(localName) { E::class.java.getEnumConstant(it, xmlValue) }

/**
 * Retrieves an enum value of attribute with given [localName]. If there is no such enum constant, the exception is wrapped in [KonsumerException]
 * with exact location and rethrown. Returns `null` if the attribute is missing. Fails if the attribute is present
 * and is outside of given range.
 * @param xmlValue extracts expected XML value from an enum constant. The XML is expected to contain one of values returned by this function.
 * Defaults to [Enum.name].
 * @throws KonsumerException if the conversion fails or the value is out of range.
 */
@KonsumerDsl
public inline fun <reified E : Enum<E>> AttributeKonsumer.getValueEnum(
    localName: String,
    crossinline xmlValue: (E) -> String = { it.name }
): E =
    getValue(localName) { E::class.java.getEnumConstant(it, xmlValue) }

public inline fun <E : Enum<E>> Class<E>.getEnumConstant(
    formattedValue: String,
    xmlValue: (E) -> String
) =
    enumConstants.firstOrNull { xmlValue(it) == formattedValue }
        ?: throw NoSuchElementException("${this.simpleName}: no enum constant for '${formattedValue}'")

@Deprecated("use getValueIntOrNull", ReplaceWith("getValueIntOrNull"))
public fun AttributeKonsumer.getValueIntOpt(localName: String,
                                               range: IntRange = Int.MIN_VALUE..Int.MAX_VALUE): Int? =
        getValueOrNull(localName) { it.toInt(range) }

/**
 * Similar to [toLong], but this function also checks that the number is in given range.
 * @throws IllegalStateException if the number is outside of given range.
 */
public fun String.toLong(validRange: LongRange): Long {
    val result: Long = toLong()
    check(result in validRange) { "The number '$result' is not in range $validRange" }
    return result
}

/**
 * Validates that the length of the string is in given [lengthRange].
 * @return this
 */
public fun String.validateLength(lengthRange: IntRange): String {
    check(length in lengthRange) { "The string length $length was outside of allowed range $lengthRange: $this" }
    return this
}

/**
 * Similar to [toBoolean], but this function checks that the string is either `"true"`, `"false"`, `"0"` or `"1"`, case-insensitive.
 * @throws IllegalStateException on invalid string input.
 */
public fun String.xmlBoolean(): Boolean = when (lowercase()) {
    "true", "1" -> true
    "false", "0" -> false
    else -> throw IllegalStateException("A boolean value of either 'true', 'false', '0' or '1' was expected, but got '$this'")
}

/**
 * Parses the [XML Date Time format](https://www.w3schools.com/xml/schema_dtypes_date.asp), for example
 * `2002-05-30T09:00:00`, `2002-05-30T09:30:10.5`, `2002-05-30T09:30:10Z` or `2002-05-30T09:30:10-06:00`.
 * @return Calendar for keeping compatibility with Android
 */
public fun String.xmlDateTime(): Calendar = DatatypeConverter.parseDateTime(this)

/**
 * Parses the [XML Date format](https://www.w3schools.com/xml/schema_dtypes_date.asp), for example
 * `2002-09-24`.
 * @return Calendar for keeping compatibility with Android
 */
public fun String.xmlDate(): Calendar = DatatypeConverter.parseDate(this)

/**
 * Parses the [XML Time format](https://www.w3schools.com/xml/schema_dtypes_date.asp), for example
 * `09:00:00`, `09:30:10.5`, `09:30:10Z` or `09:30:10-06:00`.
 * @return Calendar for keeping compatibility with Android
 */
public fun String.xmlTime(): Calendar = DatatypeConverter.parseTime(this)

/**
 * Similar to [toInt], but this function also checks that the number is in given range.
 * @throws IllegalStateException if the number is outside of given range.
 */
public fun String.toInt(validRange: IntRange): Int {
    val result = toInt()
    check(result in validRange) { "The number '$result' is not in range $validRange" }
    return result
}

public var Calendar.year: Int
    get() = get(Calendar.YEAR)
    set(value) {
        set(Calendar.YEAR, value)
    }
/**
 * Month, 1..12
 */
public var Calendar.month: Int
    get() = get(Calendar.MONTH) + 1
    set(value) {
        set(Calendar.MONTH, value - 1)
    }
/**
 * Day, 1..31
 */
public var Calendar.day: Int
    get() = get(Calendar.DAY_OF_MONTH)
    set(value) {
        set(Calendar.DAY_OF_MONTH, value)
    }
/**
 * Hour in day, 0..23
 */
public var Calendar.hour: Int
    get() = get(Calendar.HOUR_OF_DAY)
    set(value) {
        set(Calendar.HOUR_OF_DAY, value)
    }
public var Calendar.minute: Int
    get() = get(Calendar.MINUTE)
    set(value) {
        set(Calendar.MINUTE, value)
    }
public var Calendar.second: Int
    get() = get(Calendar.SECOND)
    set(value) {
        set(Calendar.SECOND, value)
    }
public var Calendar.milli: Int
    get() = get(Calendar.MILLISECOND)
    set(value) {
        set(Calendar.MILLISECOND, value)
    }

/**
 * Retrieves the date part from the calendar and formats it as XML Date, for example `2004-01-12`
 */
public fun Calendar.formatXMLDate(): String =
        "$year-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')}"

/**
 * Retrieves the time part from the calendar and formats it as XML time, for example
 * `09:00:00`, `09:30:10.5`, `09:30:10Z` or `09:30:10-06:00`.
 * @param withTimeZone if true, the time zone is included. See [formatXMLTimeZone] for more details.
 */
public fun Calendar.formatXMLTime(withTimeZone: Boolean = true): String {
    var result = "${hour.toString().padStart(2, '0')}:${minute.toString().padStart(2, '0')}:${second.toString().padStart(2, '0')}"
    if (milli != 0) {
        result += ".${milli.toString().padStart(3, '0')}"
    }
    if (withTimeZone) {
        result += timeZone.formatXMLTimeZone()
    }
    return result
}

/**
 * Formats the time zone in XML time zone format, for example `-06:00` or `+10:00`. Returns just `Z` for UTC.
 */
public fun TimeZone.formatXMLTimeZone(): String {
    var offsetInMinutes = rawOffset / 1000 / 60
    if (offsetInMinutes == 0) {
        return "Z"
    }
    return buildString {
        append(if (offsetInMinutes >= 0) '+' else '-')
        offsetInMinutes = offsetInMinutes.absoluteValue
        append((offsetInMinutes / 60).toString().padStart(2, '0'))
        append(':')
        append((offsetInMinutes % 60).toString().padStart(2, '0'))
    }
}

/**
 * Formats the caledar as XML Date time, for example `2002-05-30T09:00:00`, `2002-05-30T09:30:10.5`, `2002-05-30T09:30:10Z` or `2002-05-30T09:30:10-06:00`.
 */
public fun Calendar.formatXMLDateTime(withTimeZone: Boolean = true): String = "${formatXMLDate()}T${formatXMLTime(withTimeZone)}"

public fun String.xmlDecimal(): BigDecimal {
    require(!contains('E', true)) { "$this: scientific notation is forbidden in xsd:decimal" }
    return toBigDecimal()
}

public fun String.xmlFloat(): Float = when (this) {
    "-INF" -> Float.NEGATIVE_INFINITY
    else -> toFloat()
}

public fun String.xmlDouble(): Double = when (this) {
    "-INF" -> Double.NEGATIVE_INFINITY
    else -> toDouble()
}

public fun String.xmlHexBinary(): ByteArray = DatatypeConverter.parseHexBinary(this)

public fun ByteArray.formatXmlHexBinary(): String = DatatypeConverter.printHexBinary(this)

public fun String.xmlBase64Binary(): ByteArray {
    require(!trimEnd('=').contains('=')) { "$this: additional characters after padding character '='" }
    return DatatypeConverter.parseBase64Binary(replace(" ", ""))
}

public fun ByteArray.formatXmlBase64Binary(): String = DatatypeConverter.printBase64Binary(this)
