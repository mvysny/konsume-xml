package com.gitlab.mvysny.konsumexml.stax

/**
 * Expands [StaxEventType.EntityReference] with known text contents (such as character entities);
 * if the entity contents is not known the entity reference is left as-is.
 */
public class EntityExpandingStaxParser(public val delegate: StaxParser) : StaxParser by delegate {
    override val eventType: StaxEventType
        get() {
            val e: StaxEventType = delegate.eventType
            if (e == StaxEventType.EntityReference && text != null) {
                // both the entity text and characters can be retrieved via the `text` property
                // so it's enough if we just pretend we've received Characters instead of an EntityRef
                return StaxEventType.Characters
            }
            return e
        }
}
