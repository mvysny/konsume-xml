package com.gitlab.mvysny.konsumexml.stax

/**
 * A parser which dumps everything it reads to stdout. Example of usage:
 * ```
 * val xml: String = "..."
 * val parser: StaxParser = DebugStaxParser(StaxParserFactory.create(xml.byteInputStream(), systemId))
 * val konsumer = Konsumer(StaxReader(parser), null, KonsumerSettings())
 * ...
 * ```
 * @author mavi
 */
public class DebugStaxParser(public val delegate: StaxParser) : StaxParser by delegate {
    override fun next() {
        delegate.next()
        when (eventType) {
            StaxEventType.CData -> println("<![CDATA[$text]]>")
            StaxEventType.Characters -> println(text)
            StaxEventType.Comment -> println("<!--$text-->")
            StaxEventType.EndElement -> println("</$elementName>")
            StaxEventType.ProcessingInstruction -> println("<?$text?>")
            StaxEventType.StartElement -> println("<$elementName>")
            else -> println(eventType)
        }
    }
}