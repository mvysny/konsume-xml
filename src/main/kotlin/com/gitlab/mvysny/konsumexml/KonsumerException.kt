package com.gitlab.mvysny.konsumexml

import com.gitlab.mvysny.konsumexml.stax.Location
import javax.xml.namespace.QName

/**
 * Konsumer exception occurred at [location].
 */
public class KonsumerException(
        @Suppress("MemberVisibilityCanBePrivate") public val location: Location,
        @Suppress("CanBeParameter") public val elementName: QName?,
        msg: String,
        cause: Throwable? = null
) : RuntimeException("$location, in element <${elementName?.getFullName()}>: $msg", cause)
