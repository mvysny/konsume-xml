package com.gitlab.mvysny.konsumexml

import javax.xml.namespace.QName

/**
 * An element name matcher, matches elements with particular name. Examples:
 * * `of("title")` matches elements with exactly given tag name
 * * `of("name", "surname", "title")` matches elements with any of given tag names
 * * `any()` matches any element with any name. You can also use the convenience constant [anyName].
 */
public sealed class Names {
    /**
     * Checks whether this matcher matches element with given [localName] and [namespace].
     */
    public abstract fun accepts(localName: String, namespace: String): Boolean

    /**
     * Checks whether this matcher matches element with given [name].
     */
    public fun accepts(name: QName): Boolean = accepts(name.localPart, name.namespaceURI)

    /**
     * Matches exactly one [localName].
     */
    private data class One(val localName: String) : Names() {
        override fun accepts(localName: String, namespace: String): Boolean = localName == this.localName
        override fun toString() = "'$localName'"
    }

    /**
     * Matches any of given set of [localNames].
     */
    private data class Multiple(val localNames: Set<String>) : Names() {
        override fun accepts(localName: String, namespace: String): Boolean = localNames.contains(localName)
        override fun toString() = "'$localNames'"
    }

    /**
     * Matches any name.
     */
    private object Any : Names() {
        override fun accepts(localName: String, namespace: String): Boolean = true
        override fun toString(): String = "any"
    }

    public companion object {
        /**
         * Returns a matcher which matches elements with exactly this [localName].
         */
        public fun of(localName: String): Names = One(localName)

        /**
         * Returns a matcher which matches elements with any of given [localNames].
         */
        public fun of(vararg localNames: String): Names {
            check(localNames.isNotEmpty()) { "name cannot be empty" }
            return when (localNames.size) {
                1 -> One(localNames[0])
                else -> Multiple(localNames.toSet())
            }
        }

        /**
         * Returns a matcher which matches elements with any of given [localNames].
         */
        public fun of(localNames: Set<String>): Names {
            check(localNames.isNotEmpty()) { "name cannot be empty" }
            return when (localNames.size) {
                1 -> One(localNames.first())
                else -> Multiple(localNames.toSet())
            }
        }

        /**
         * Returns a matcher which matches element with any name.
         */
        public fun any(): Names = Any
    }
}

/**
 * Matches any name.
 */
public val anyName: Names = Names.any()
