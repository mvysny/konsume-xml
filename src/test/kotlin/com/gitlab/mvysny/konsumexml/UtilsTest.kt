package com.gitlab.mvysny.konsumexml

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import javax.xml.namespace.QName
import kotlin.test.expect

class UtilsTest {
    @Test fun existsClass() {
        expect(true) { existsClass("java.lang.Object") }
        expect(false) { existsClass("com.foo.NonExistent") }
    }

    @Nested inner class QNameTests {
        @Test fun tagName() {
            expect("foo") { QName("foo").tagName }
            expect("foo") { QName("bar", "foo").tagName }
            expect("baz:foo") { QName("bar", "foo", "baz").tagName }
        }
        @Test fun getFullName() {
            expect("foo") { QName("foo").getFullName() }
            expect("{bar}foo") { QName("bar", "foo").getFullName() }
            expect("{bar}baz:foo") { QName("bar", "foo", "baz").getFullName() }
        }
    }
}
