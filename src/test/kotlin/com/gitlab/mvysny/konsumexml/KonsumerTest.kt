package com.gitlab.mvysny.konsumexml

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import readmeexamples.Resource
import java.lang.IllegalStateException
import kotlin.test.expect

class KonsumerTest {
    @Nested inner class JavaxXmlTests : AbstractKonsumerTests(XmlParserType.JAVAX_XML)
    @Nested inner class ComBeaTests : AbstractKonsumerTests(XmlParserType.COM_BEA)
    @Nested inner class XmlPullTests : AbstractKonsumerTests(XmlParserType.XMLPULL)
}

abstract class AbstractKonsumerTests(xmlParserType: XmlParserType) : AbstractXmlParserTest(xmlParserType) {
        @Test fun smoke() {
            data class Employee(val id: Int, val name: String, val salary: Int)

            fun employeeXml(k: Konsumer): Employee {
                k.checkCurrent("employee")
                return Employee(k.childInt("id"), k.childText("name"), k.childInt("salary"))
            }

            val employee: Employee = """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.trimIndent().konsumeXml().child("employee") { employeeXml(this) }
            expect(Employee(1, "Alba", 100)) { employee }
        }

        @Test fun testToString() {
            """<employee xmlns:foo="foobar"><foo:id>1</foo:id><name>Alba</name></employee>""".konsumeXml().apply {
                    expect("line 1 column 1 at null null, in element <null>") { toString() }
                    child("employee") {
                        expect("line 1 column 30 at null null, in element <employee>") { toString() }
                        child("id") {
                            expect("line 1 column 38 at null null, in element <{foobar}foo:id>") { toString() }
                            text()
                        }
                        child("name") {
                            expect("line 1 column 54 at null null, in element <name>") { toString() }
                            text()
                        }
                    }
                }
        }

        @Test fun `current element name`() {
            """<employee>
                <id>1</id>
                <name>Alba</name>
            </employee>
            """.konsumeXml().apply {
                expect(null) { elementName }
                child("employee") {
                    expect("employee") { name.toString() }
                    child("id") {
                        expect("id") { name.toString() }
                        text()
                    }
                    child("name") {
                        expect("name") { name.toString() }
                        text()
                    }
                }
            }
        }

        @Nested inner class `missing elements`() {
            private val k: Konsumer = """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.konsumeXml()

            @Test fun root() {
                expectThrows(KonsumerException::class, "at null null, in element <null>: Expected element 'foo' but got 'employee'") {
                    k.child("foo") {}
                }
            }
            @Test fun child() {
                expectThrows(KonsumerException::class, "at null null, in element <employee>: Expected element 'name' but got 'id'") {
                    k.child("employee") {
                        child("name") {}
                    }
                }
            }
            @Test fun childOpt() {
                k.child("employee") {
                    expect(null) { childIntOpt("name") }
                    expect(1) { childIntOpt("id") }
                    expect("Alba") { childTextOrNull("name") }
                    expect(100) { childIntOpt("salary") }
                }
            }
        }

        @Nested inner class `redundant elements` {
            private val k: Konsumer = """
                    <employee>
                        <id>1</id>
                        <name>Alba</name>
                        <salary>100</salary>
                    </employee>
                """.konsumeXml()

            @Test fun `root not fully consumed`() {
                expectThrows(KonsumerException::class, "at null null, in element <employee>: Expected END_ELEMENT but got START_ELEMENT: 'id'") {
                    k.child("employee") {}
                }
            }
        }

        @Nested inner class `can't use parent konsumer until child konsumer finishes` {
            @Test fun `text()-child()`() {
                """<employee>
                <id>1</id>
            </employee>
            """.konsumeXml().apply {
                    val parent = this
                    child("employee") {
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.text()
                        }
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.child("id") {}
                        }
                        expect(1) { childInt("id") }
                    }
                }
            }
            @Test fun attributes() {
                "<employee><id>1</id></employee>".konsumeXml().apply {
                    val parent = this
                    child("employee") {
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.attributes
                        }
                        expect(1) { childInt("id") }
                    }
                }
            }
            @Test fun children() {
                "<employee><id>1</id></employee>".konsumeXml().apply {
                    val parent = this
                    child("employee") {
                        expectThrows(IllegalStateException::class, "A child konsumer of 'employee' is ongoing, cannot use this consumer of 'null' until the child konsumer finishes") {
                            parent.children("employee") {}
                        }
                        expect(1) { childInt("id") }
                    }
                }
            }
        }

        @Nested inner class text {
            @Nested inner class `text conversion` : AbstractTextConversionTests()
            @Test fun unconsumed() {
                expectThrows(KonsumerException::class, "at null null, in element <e>: Expected END_ELEMENT but there is unconsumed text: 'foo'") {
                    """<e>foo</e>""".konsumeXml().child("e") {}
                }
                expectThrows(KonsumerException::class, "line 1 column 8 at null null, in element <e>: Expected element 'b' but there is unconsumed text: 'foo'") {
                    """<e>foo<b/>bar</e>""".konsumeXml().child("e") { child("b") {} }
                }
            }
            @Nested inner class whitespaces : AbstractWhitespacesTests()
        }

        @Nested inner class children : AbstractChildrenTests()
        @Nested inner class stream : AbstractStreamTests()
        @Nested inner class skipContents : AbstractSkipContentsTests()
        @Nested inner class `special node types` {
            @Test fun `character entity`() {
                // test for https://gitlab.com/mvysny/konsume-xml/issues/4
                """<a>&#x2117;</a>""".konsumeXml().use {
                    expect("℗") { it.childText("a") }
                }
            }
            @Test fun Comment() {
                """<a><!-- ignored --></a>""".konsumeXml().use {
                    expect("") { it.childText("a") }
                }
            }
            @Test fun CDATA() {
                """<a><![CDATA[foo]]></a>""".konsumeXml().use {
                    expect("foo") { it.childText("a") }
                }
            }
            @Test fun PI() {
                """<a><?PITarget PIContent?></a>""".konsumeXml().use {
                    expect("") { it.childText("a") }
                }
            }
        }

        @Nested inner class `exceptions wrapped properly` {
            @Test fun `simple root element`() {
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <a>: simulated") {
                    """<a/>""".konsumeXml().child("a") { throw RuntimeException("simulated") }
                }
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <a>: simulated") {
                    """<a/>""".konsumeXml().children("a") { throw RuntimeException("simulated") }
                }
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <a>: simulated") {
                    """<a/>""".konsumeXml().childOrNull<Nothing>("a") { throw RuntimeException("simulated") }
                }
            }
        }

        @Nested inner class `mixed contents` : AbstractMixedContentsTests()
        @Nested inner class namespaces : AbstractNamespaceTests()
}

abstract class AbstractMixedContentsTests {
    @Test fun `children mixed with text`() {
        expectThrows(KonsumerException::class, "at null null, in element <employee>: Expected element 'b' but there is unconsumed text: 'a'") {
            """<employee>
                    a <b>bold</b> text with <b>bold</b> contents
                </employee>
            """.konsumeXml().child("employee") {
                childrenText("b")
            }
        }
    }
    @Test fun `text consumed until the end`() {
        "<p>a text</p>".konsumeXml().child("p") {
            expect("a text") { text(failOnElement = false) }
            expect(true) { isFinished }
        }
    }
    @Test fun `to markdown example`() {
        fun Konsumer.toMarkdown(): String = buildString {
            while (!isFinished) {
                // read some text until a child element is encountered
                append(text(Whitespace.replace, failOnElement = false))
                // if there is no more content, bail out
                if (isFinished) {
                    break
                }
                // an element has been encountered, read it.
                child(Names.of("b", "i", "p")) {
                    // reading the contents of the child element
                    val markdown = when (localName) {
                        "b" -> "*${text()}*"
                        "i" -> "**${text()}**"
                        else -> toMarkdown()
                    }
                    append(markdown)
                }
            }
        }
        expect("a *bold* text with **italic** contents") {
            "<p>a <b>bold</b> text with <i>italic</i> contents</p>".konsumeXml().child("p") {
                toMarkdown()
            }
        }
    }
}

abstract class AbstractSkipContentsTests {
    @Test fun `root empty element`() {
        """<a/>""".konsumeXml().use {
            it.skipContents()
        }
    }
    @Test fun `root element`() {
        """<a><b/></a>""".konsumeXml().use {
            it.child("a") { skipContents() }
        }
    }
    @Test fun `child empty element`() {
        """<a><b/></a>""".konsumeXml().use {
            it.child("a") {
                child("b") { skipContents() }
            }
        }
    }
    @Test fun `child element with text`() {
        """<a><b>foo</b></a>""".konsumeXml().use {
            it.child("a") {
                child("b") { skipContents() }
            }
        }
    }
    @Test fun `child element with elements`() {
        """<a><b><c><d>bar</d></c></b></a>""".konsumeXml().use {
            it.child("a") {
                child("b") { skipContents() }
            }
        }
    }
    @Test fun `child empty elements`() {
        """<a><b/><b/><b/></a>""".konsumeXml().use {
            it.child("a") {
                children("b") { skipContents() }
            }
        }
    }
    @Test fun `child elements with text`() {
        """<a><b/><b>foo</b><b>bar</b></a>""".konsumeXml().use {
            it.child("a") {
                children("b") { skipContents() }
            }
        }
    }
    @Test fun `child empty optional element`() {
        """<a/>""".konsumeXml().use {
            it.child("a") {
                childOrNull("b") { skipContents() }
            }
        }
        """<a><b/></a>""".konsumeXml().use {
            it.child("a") {
                childOrNull("b") { skipContents() }
            }
        }
    }
    @Test fun `child optional element with text`() {
        """<a><b>foo</b></a>""".konsumeXml().use {
            it.child("a") {
                children("b") { skipContents() }
            }
        }
        """<a/>""".konsumeXml().use {
            it.child("a") {
                children("b") { skipContents() }
            }
        }
    }
}

abstract class AbstractWhitespacesTests {
    @Test fun `collapses by default`() {
        expect("foo") { """<e>foo</e>""".konsumeXml().childText("e") }
        expect("foo") { """<e>foo </e>""".konsumeXml().childText("e") }
        expect("foo") { """<e> foo</e>""".konsumeXml().childText("e") }
        expect("foo") { """<e> foo </e>""".konsumeXml().childText("e") }
        expect("foo") { """<e>&#x9;foo&#x9;&#x9;</e>""".konsumeXml().childText("e") }
        expect("foo") { "<e>\tfoo\t\t</e>".konsumeXml().childText("e") }
        expect("foo") { """<e> foo  </e>""".konsumeXml().childText("e") }
        expect("foo") {
            "<e> foo  \n</e>".konsumeXml().childText("e")
        }
        expect("foo bar baz") { "<e>   foo\t\t\n  bar    baz   </e>".konsumeXml().childText("e") }
        // test that consecutive text nodes are processed identically as one huge text node: https://gitlab.com/mvysny/konsume-xml/-/issues/22
        expect("foo bar baz") {
            "<e>   <![CDATA[foo]]>\t\t\n <![CDATA[ bar ]]><![CDATA[   baz   ]]></e>".konsumeXml().childText("e")
        }
    }
    @Test fun `collapse no trim`() {
        expect("foo") { """<e>foo</e>""".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect("foo ") { """<e>foo </e>""".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect(" foo") { """<e> foo</e>""".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect(" foo ") { """<e> foo </e>""".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect(" foo ") { """<e>&#x9;foo&#x9;&#x9;</e>""".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect(" foo ") { "<e>\tfoo\t\t</e>".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect(" foo ") { """<e> foo  </e>""".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        expect(" foo ") {
            "<e> foo  \n</e>".konsumeXml().childText("e", Whitespace.collapse_no_trim)
        }
        expect(" foo bar baz ") { "<e>   foo&#x9;&#x9;\n  bar    baz   </e>".konsumeXml().childText("e", Whitespace.collapse_no_trim) }
        // test that consecutive text nodes are processed identically as one huge text node: https://gitlab.com/mvysny/konsume-xml/-/issues/22
        expect(" foo bar baz ") {
            "<e>   <![CDATA[foo]]>\t\t\n <![CDATA[ bar ]]><![CDATA[   baz   ]]></e>".konsumeXml().childText("e", Whitespace.collapse_no_trim)
        }
    }
    @Test fun preserve() {
        expect("foo") { """<e>foo</e>""".konsumeXml().childText("e", Whitespace.preserve) }
        expect("foo ") { """<e>foo </e>""".konsumeXml().childText("e", Whitespace.preserve) }
        expect(" foo") { """<e> foo</e>""".konsumeXml().childText("e", Whitespace.preserve) }
        expect(" foo ") { """<e> foo </e>""".konsumeXml().childText("e", Whitespace.preserve) }
        expect("\tfoo\t\t") {
            """<e>&#x9;foo&#x9;&#x9;</e>""".konsumeXml().childText("e", Whitespace.preserve)
        }
        expect("\tfoo\t\t") { "<e>\tfoo\t\t</e>".konsumeXml().childText("e", Whitespace.preserve) }
        expect(" foo  \n") {
            "<e> foo  \n</e>".konsumeXml().childText("e", Whitespace.preserve)
        }
        expect("   foo\t\t\n  bar    baz   ") { "<e>   foo&#x9;&#x9;\n  bar    baz   </e>".konsumeXml().childText("e", Whitespace.preserve) }
        // test that consecutive text nodes are processed identically as one huge text node: https://gitlab.com/mvysny/konsume-xml/-/issues/22
        expect("   foo\t\t\n  bar    baz   ") {
            "<e>   <![CDATA[foo]]>\t\t\n <![CDATA[ bar ]]><![CDATA[   baz   ]]></e>".konsumeXml().childText("e", Whitespace.preserve)
        }
    }
    @Test fun replace() {
        expect("foo") { """<e>foo</e>""".konsumeXml().childText("e", Whitespace.replace) }
        expect("foo ") { """<e>foo </e>""".konsumeXml().childText("e", Whitespace.replace) }
        expect(" foo") { """<e> foo</e>""".konsumeXml().childText("e", Whitespace.replace) }
        expect(" foo ") { """<e> foo </e>""".konsumeXml().childText("e", Whitespace.replace) }
        expect(" foo  ") {
            """<e>&#x9;foo&#x9;&#x9;</e>""".konsumeXml().childText("e", Whitespace.replace)
        }
        expect(" foo  ") { "<e>\tfoo\t\t</e>".konsumeXml().childText("e", Whitespace.replace) }
        expect(" foo  ") {
            """<e> foo  </e>""".konsumeXml().childText("e", Whitespace.replace)
        }
        expect(" foo   ") {
            "<e> foo  \n</e>".konsumeXml().childText("e", Whitespace.replace)
        }
        expect("   foo     bar    baz   ") { "<e>   foo&#x9;&#x9;\n  bar    baz   </e>".konsumeXml().childText("e", Whitespace.replace) }
        // test that consecutive text nodes are processed identically as one huge text node: https://gitlab.com/mvysny/konsume-xml/-/issues/22
        expect("   foo     bar    baz   ") {
            "<e>   <![CDATA[foo]]>\t\t\n <![CDATA[ bar ]]><![CDATA[   baz   ]]></e>".konsumeXml().childText("e", Whitespace.replace)
        }
    }
}

abstract class AbstractStreamTests {
    @Test fun `simple stream`() {
        """
                <resources>
                    <string name="name1">content1</string>
                    <string name="name2">content2</string>
                    <string name="name3">content3</string>
                </resources>
                """.trimIndent().konsumeXml().use {
            val konsumer = it.nextElement(Names.of("resources"), true)!!
            val resources: Sequence<Resource> = konsumer.childrenSequence("string") { Resource.xml(this) }
            expect(3) { resources.count() }
        }
    }

    @Test fun `lower limit`() {
        """<resources></resources>""".konsumeXml().use {
            val konsumer = it.nextElement(Names.of("resources"), true)!!
            val resources: Sequence<Resource> = konsumer.childrenSequence("string", 3) { Resource.xml(this) }
            expectThrows(KonsumerException::class, "at null null, in element <resources>: At least 3 of element 'string' was expected, but only 0 encountered") {
                resources.count()
            }
        }
    }

    @Test fun `lower limit 2`() {
        """<resources><string name="name1">content1</string></resources>""".konsumeXml().use {
            val konsumer = it.nextElement(Names.of("resources"), true)!!
            val resources: Sequence<Resource> = konsumer.childrenSequence("string", 3) { Resource.xml(this) }
            expectThrows(KonsumerException::class, "at null null, in element <resources>: At least 3 of element 'string' was expected, but only 1 encountered") {
                resources.count()
            }
        }
    }

    @Test fun `lower limit 3`() {
        """<resources><string name="name1">content1</string></resources>""".konsumeXml().use {
            val konsumer = it.nextElement(Names.of("resources"), true)!!
            val resources: Sequence<Resource> = konsumer.childrenSequence("string", 1) { Resource.xml(this) }
            expect(1) { resources.count() }
        }
    }
}

abstract class AbstractTextConversionTests {
    @Nested inner class toInt {
        @Test fun `simple conversion`() {
            expect(1) {
                """<employee>
                                <id>1</id>
                            </employee>""".konsumeXml().child("employee") {
                    childInt("id")
                }
            }
        }
        @Test fun `failed conversion`() {
            expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                    childInt("id")
                }
            }
        }
        @Test fun `failed conversion Opt`() {
            expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                    childIntOpt("id")
                }
            }
        }
        @Test fun `failed range test`() {
            expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                    childInt("id", 10..15)
                }
            }
        }
        @Test fun `failed range test Opt`() {
            expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                    childIntOpt("id", 10..15)
                }
            }
        }
        @Test fun `range test skipped for missing element`() {
            """<employee/>""".konsumeXml().child("employee") {
                expect(null) { childIntOpt("id", 10..15) }
            }
        }
    }
    @Nested inner class toLong {
        @Test fun `simple conversion`() {
            expect(1) {
                """<employee>
                                <id>1</id>
                            </employee>""".konsumeXml().child("employee") {
                    childText("id") { it.toLong() }
                }
            }
        }
        @Test fun `failed conversion`() {
            expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                    childText("id") { it.toLong() }
                }
            }
        }
        @Test fun `failed conversion Opt`() {
            expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': For input string: \"invalid\"") {
                """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                    childTextOrNull("id") { it.toLong() }
                }
            }
        }
        @Test fun `failed range test`() {
            expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                    childText("id") { it.toLong(10L..15) }
                }
            }
        }
        @Test fun `failed range test Opt`() {
            expectThrows(KonsumerException::class, "line 1 column 21 at null null, in element <id>: Failed to convert '1': The number '1' is not in range 10..15") {
                """<employee><id>1</id></employee>""".konsumeXml().child("employee") {
                    childTextOrNull("id") { it.toLong(10L..15) }
                }
            }
        }
        @Test fun `range test skipped for missing element`() {
            """<employee/>""".konsumeXml().child("employee") {
                expect(null) { childTextOrNull("id") { it.toLong(10L..15) } }
            }
        }
    }
    @Nested inner class xmlDateTime {
        @Test fun `simple conversion`() {
            val dateTime = """<employee><dob>2002-05-30T09:00:00</dob></employee>""".konsumeXml().child("employee") {
                childText("dob") { it.xmlDateTime() }
            }
            expect("2002-05-30T09:00:00") { dateTime.formatXMLDateTime(false) }
        }
        @Test fun `failed conversion`() {
            expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': invalid") {
                """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                    childText("id") { it.xmlDateTime() }
                }
            }
        }
        @Test fun `failed conversion Opt`() {
            expectThrows(KonsumerException::class, "line 1 column 27 at null null, in element <id>: Failed to convert 'invalid': invalid") {
                """<employee><id>invalid</id></employee>""".konsumeXml().child("employee") {
                    childTextOrNull("id") { it.xmlDateTime() }
                }
            }
        }
    }
}

abstract class AbstractChildrenTests {
    @Test fun smoke() {
        """
            <employee>
                <id>1</id>
                <name>Alba</name>
                <salary>100</salary>
            </employee>
            """.trimIndent().konsumeXml().child("employee") {
            expectList(1) { childrenInt("id") }
            expectList("Alba") { childrenText("name") }
            expectList(100) { childrenInt("salary") }
        }
    }
    @Test fun `multiple children at the start`() {
        """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
            expectList("", "", "") { childrenText("a") }
            expect("") { childText("b") }
        }
    }
    @Test fun `missing children at the start`() {
        """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
            expectList() { childrenText("c") }
            expectList("", "", "") { childrenText("a") }
            expect("") { childText("b") }
        }
    }
    @Test fun minCount() {
        expectThrows(KonsumerException::class, "line 1 column 20 at null null, in element <r>: At least 5 of element 'a' was expected, but only 3 encountered") {
            """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
                childrenText("a", 5)
            }
        }
    }
    @Nested inner class maxCount {
        @Test fun `0 doesn't consume anything`() {
            """<r><a/><a/><a/><b/></r>""".konsumeXml().child("r") {
                expectList() { childrenText("a", 0, 0) }
                expectList("") { childrenText("a", 0, 1) }
                expectList("", "") { childrenText("a", 0, 2) }
                child("b") {}
            }
        }
    }
}

abstract class AbstractNamespaceTests {
    @Test fun simple() {
        """<foo:e xmlns:foo="foobar"/>""".konsumeXml().apply {
            child("e") {
                expect("{foobar}e") { name.toString() }
                expect("e") { localName }
                expect("foo:e") { tagName }
            }
        }
    }
    @Test fun nested() {
        """<e xmlns:foo="foobar"><foo:a/></e>""".konsumeXml().apply {
            child("e") {
                expect("e") { name.toString() }
                expect("e") { localName }
                expect("e") { tagName }

                child("a") {
                    expect("{foobar}a") { name.toString() }
                    expect("a") { localName }
                    expect("foo:a") { tagName }
                }
            }
        }
    }

    @Test fun `nested 2`() {
        """<foo:e xmlns:foo="foobar"><a/></foo:e>""".konsumeXml().apply {
            child("e") {
                expect("{foobar}e") { name.toString() }
                expect("e") { localName }
                expect("foo:e") { tagName }

                child("a") {
                    expect("a") { name.toString() }
                    expect("a") { localName }
                    expect("a") { tagName }
                }
            }
        }
    }
}
