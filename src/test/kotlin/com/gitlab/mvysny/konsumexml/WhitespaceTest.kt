package com.gitlab.mvysny.konsumexml

import org.junit.jupiter.api.Test
import kotlin.test.expect

class WhitespaceTest {
    @Test fun preserve() {
        expect("") { Whitespace.preserve.process("") }
        expect(" ") { Whitespace.preserve.process(" ") }
        expect("    ") { Whitespace.preserve.process("    ") }
        expect("\t") { Whitespace.preserve.process("\t") }
        expect("\r\n") { Whitespace.preserve.process("\r\n") }
        expect("\n") { Whitespace.preserve.process("\n") }
        expect("  \n  ") { Whitespace.preserve.process("  \n  ") }
        expect("\t  \n") { Whitespace.preserve.process("\t  \n") }
        expect("\t\taa\t\t") { Whitespace.preserve.process("\t\taa\t\t") }
        expect("aa    bb") { Whitespace.preserve.process("aa    bb") }
        expect("  aa bb  ") { Whitespace.preserve.process("  aa bb  ") }
        expect("\t\taa\tbb\n\n") { Whitespace.preserve.process("\t\taa\tbb\n\n") }
        expect("aa\t\t\tbb") { Whitespace.preserve.process("aa\t\t\tbb") }
    }
    @Test fun replace() {
        expect("") { Whitespace.replace.process("") }
        expect(" ") { Whitespace.replace.process(" ") }
        expect("    ") { Whitespace.replace.process("    ") }
        expect(" ") { Whitespace.replace.process("\t") }
        expect("  ") { Whitespace.replace.process("\r\n") }
        expect(" ") { Whitespace.replace.process("\n") }
        expect("     ") { Whitespace.replace.process("  \n  ") }
        expect("    ") { Whitespace.replace.process("\t  \n") }
        expect("  aa  ") { Whitespace.replace.process("\t\taa\t\t") }
        expect("aa    bb") { Whitespace.replace.process("aa    bb") }
        expect("  aa bb  ") { Whitespace.replace.process("  aa bb  ") }
        expect("  aa bb  ") { Whitespace.replace.process("\t\taa\tbb\n\n") }
        expect("aa   bb") { Whitespace.replace.process("aa\t\t\tbb") }
    }
    @Test fun collapse() {
        expect("") { Whitespace.collapse.process("") }
        expect("") { Whitespace.collapse.process(" ") }
        expect("") { Whitespace.collapse.process("    ") }
        expect("") { Whitespace.collapse.process("\t") }
        expect("") { Whitespace.collapse.process("\r\n") }
        expect("") { Whitespace.collapse.process("\n") }
        expect("") { Whitespace.collapse.process("  \n  ") }
        expect("") { Whitespace.collapse.process("\t  \n") }
        expect("aa") { Whitespace.collapse.process("\t\taa\t\t") }
        expect("aa bb") { Whitespace.collapse.process("aa    bb") }
        expect("aa bb") { Whitespace.collapse.process("  aa bb  ") }
        expect("aa bb") { Whitespace.collapse.process("\t\taa\tbb\n\n") }
        expect("aa bb") { Whitespace.collapse.process("aa\t\t\tbb") }
    }
}
