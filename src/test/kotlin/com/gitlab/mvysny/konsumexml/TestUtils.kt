package com.gitlab.mvysny.konsumexml

import com.bea.xml.stream.MXParserFactory
import com.gitlab.mvysny.konsumexml.stax.*
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import java.io.ByteArrayInputStream
import java.io.InputStream
import javax.xml.stream.XMLInputFactory
import kotlin.reflect.KClass
import kotlin.test.assertFailsWith
import kotlin.test.expect

/**
 * Reads this string as a XML. Dumps XML events to stdout.
 */
fun String.debugKonsume(): Konsumer {
    val parser: StaxParser = DebugStaxParser(StaxParserFactory.create(byteInputStream(), null))
    val konsumer = Konsumer(StaxReader(parser), null, KonsumerSettings())
    return konsumer
}

/**
 * Makes sure that given [xmlParser] is using when the tests are run.
 */
abstract class AbstractXmlParserTest(val xmlParser: XmlParserType) {
    @BeforeEach fun configureParser() {
        xmlParser.apply()
    }
    @AfterEach fun cleanupParser() { StaxParserFactory.factory = StaxParserFactory.defaultFactory }
}

enum class XmlParserType {
    /**
     * The Java JDK built-in javax.xml.stream parser.
     */
    JAVAX_XML {
        override val xmlParserFactory: XmlParserFactory get() = { stream, systemId ->
            val factory: XMLInputFactory = XMLInputFactory.newInstance()
            // make sure it's coming from JDK and it's not the com.bea stax parser
            expect(false, "using com.bea stax accidentally! ${factory.javaClass}") { factory.javaClass.name.startsWith("com.bea") }
            JavaxXmlStreamStaxParser.create(stream, systemId, factory)
        }
    },

    /**
     * The `com.bea` stax parser. See https://gitlab.com/mvysny/konsume-xml/-/issues/12
     */
    COM_BEA {
        override val xmlParserFactory: (InputStream, String?) -> StaxParser
            get() = { stream, systemId ->
                JavaxXmlStreamStaxParser.create(stream, systemId, MXParserFactory())
            }
    },

    /**
     * the `org.xmlpull.v1` parser `org.ogce:xpp3:1.1.6` (used by Android)
     */
    XMLPULL {
        override val xmlParserFactory: (InputStream, String?) -> StaxParser
            get() = { stream, systemId -> OrgXmlpullStaxParser.create(stream, systemId) }
    };

    abstract val xmlParserFactory: XmlParserFactory

    /**
     * Makes sure this XML parser is used by konsume-xml from now on.
     */
    fun apply() {
        StaxParserFactory.factory = xmlParserFactory
    }
}

// creates a StaxReader reading XML stored in this string.
fun String.staxReader(): StaxReader {
    val stream: ByteArrayInputStream = byteInputStream()
    return StaxReader(StaxParserFactory.create(stream), stream)
}

/**
 * Expects that [actual] list of objects matches [expected] list of objects. Fails otherwise.
 */
fun <T> expectList(vararg expected: T, actual: ()->List<T>) {
    expect(expected.toList(), actual)
}


/**
 * Expects that given block fails with an exception of type [clazz] (or its subtype).
 *
 * Note that this is different from [assertFailsWith] since this function
 * also asserts on [Throwable.message].
 * @param expectMessage optional substring which the [Throwable.message] must contain.
 * @throws AssertionError if the block completed successfully or threw some other exception.
 * @return the exception thrown, so that you can assert on it.
 */
fun <T: Throwable> expectThrows(clazz: KClass<out T>, expectMessage: String = "", block: ()->Unit): T {
    val ex = assertFailsWith(clazz, block)
    if (!(ex.message ?: "").contains(expectMessage)) {
        throw AssertionError("${clazz.javaObjectType.name} message: Expected '$expectMessage' but was '${ex.message}'", ex)
    }
    return ex
}

enum class DemoEnum {
    total, error, passed, failed
}