package com.gitlab.mvysny.konsumexml

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import javax.xml.XMLConstants
import kotlin.test.expect

class AttributeKonsumerTest {
    @Nested inner class JavaxXmlTests : AbstractAttributeKonsumerTests(XmlParserType.JAVAX_XML)
    @Nested inner class ComBeaTests : AbstractAttributeKonsumerTests(XmlParserType.COM_BEA)
    @Nested inner class XmlPullTests : AbstractAttributeKonsumerTests(XmlParserType.XMLPULL)
}

abstract class AbstractAttributeKonsumerTests(xmlParserType: XmlParserType) : AbstractXmlParserTest(xmlParserType) {
        @Test fun simple() {
            """<e id="5"/>""".konsumeXml().child("e") {
                expect("5") { attributes["id"] }
            }
        }
        @Nested inner class `attribute names` {
            @Test fun empty() {
                """<e/>""".konsumeXml().child("e") {
                    expectList() { attributes.names }
                    expectList() { attributes.fullNames }
                }
            }
            @Test fun simple() {
                """<e id="5"/>""".konsumeXml().child("e") {
                    expectList("id") { attributes.names.map { it.toString() } }
                    expectList("id") { attributes.fullNames }
                }
            }
            @Test fun namespaced() {
                """<e foo:id="5" xmlns:foo="bar"/>""".konsumeXml().child("e") {
                    expectList("{bar}id") { attributes.names.map { it.toString() } }
                    expectList("{bar}foo:id") { attributes.fullNames }
                }
            }
            @Test fun `parent element with namespace`() {
                """<foo:e xmlns:foo="bar" id="5"/>""".konsumeXml().child("e") {
                    expectList("id") { attributes.names.map { it.toString() } }
                    expectList("id") { attributes.fullNames }
                }
            }
            @Test fun `xml returned, xmlns not returned`() {
                """<e xml:lang="en" xmlns:atom="foo"/>""".konsumeXml().child("e") {
                    expectList("{http://www.w3.org/XML/1998/namespace}xml:lang") { attributes.fullNames }
                }
            }
        }
        @Nested inner class toInt {
            @Test fun simple() {
                """<e id="5"/>""".konsumeXml().child("e") {
                    expect(5) { attributes.getValueInt("id") }
                }
            }
            @Test fun `out of range`() {
                expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                    """<e id="5"/>""".konsumeXml().child("e") {
                        attributes.getValueInt("id", 10..15)
                    }
                }
            }
            @Test fun `out of range Opt`() {
                expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                    """<e id="5"/>""".konsumeXml().child("e") {
                        attributes.getValueIntOrNull("id", 10..15)
                    }
                }
            }
            @Test fun `failed type conversion provides location properly`() {
                expectThrows(KonsumerException::class, "line 1 column 14 at null null, in element <e>: Failed to convert the value 'foo' of attribute 'id': 'For input string: \"foo\"'") {
                    """<e id="foo"/>""".konsumeXml().child("e") {
                        attributes.getValueInt("id")
                    }
                }
            }
            @Test fun `returns null even if outside of range`() {
                """<e/>""".konsumeXml().child("e") {
                    expect(null) { attributes.getValueIntOrNull("id", 10..15) }
                }
            }
        }
    @Nested inner class toEnum {
        @Test fun simple() {
            """<e status="total"/>""".konsumeXml().child("e") {
                expect(DemoEnum.total) { attributes.getValueEnum("status") }
            }
        }
        @Test fun `out of range`() {
            expectThrows(KonsumerException::class, "line 1 column 18 at null null, in element <e>: Failed to convert the value 'foo' of attribute 'status': 'DemoEnum: no enum constant for 'foo'") {
                """<e status="foo"/>""".konsumeXml().child("e") {
                    attributes.getValueEnum<DemoEnum>("status")
                }
            }
        }
        @Test fun `out of range Opt`() {
            expectThrows(KonsumerException::class, "line 1 column 16 at null null, in element <e>: Failed to convert the value '5' of attribute 'status': 'DemoEnum: no enum constant for '5'") {
                """<e status="5"/>""".konsumeXml().child("e") {
                    attributes.getValueEnumOrNull<DemoEnum>("status")
                }
            }
        }
        @Test fun `returns null even if outside of range`() {
            """<e/>""".konsumeXml().child("e") {
                expect(null) { attributes.getValueEnumOrNull<DemoEnum>("id") }
            }
        }
        @Nested inner class customFormatter {
            @Test fun simple() {
                """<e status="TOTAL"/>""".konsumeXml().child("e") {
                    expect(DemoEnum.total) { attributes.getValueEnum("status") { it.name.uppercase() } }
                }
            }
            @Test fun `out of range`() {
                expectThrows(KonsumerException::class, "line 1 column 18 at null null, in element <e>: Failed to convert the value 'foo' of attribute 'status': 'DemoEnum: no enum constant for 'foo'") {
                    """<e status="foo"/>""".konsumeXml().child("e") {
                        attributes.getValueEnum<DemoEnum>("status") { it.name.uppercase() }
                    }
                }
            }
            @Test fun `out of range Opt`() {
                expectThrows(KonsumerException::class, "line 1 column 16 at null null, in element <e>: Failed to convert the value '5' of attribute 'status': 'DemoEnum: no enum constant for '5'") {
                    """<e status="5"/>""".konsumeXml().child("e") {
                        attributes.getValueEnumOrNull<DemoEnum>("status") { it.name.uppercase() }
                    }
                }
            }
            @Test fun `returns null even if outside of range`() {
                """<e/>""".konsumeXml().child("e") {
                    expect(null) { attributes.getValueEnumOrNull<DemoEnum>("status") { it.name.uppercase() } }
                }
            }
        }
    }
        @Nested inner class toLong {
            @Test fun simple() {
                """<e id="5"/>""".konsumeXml().child("e") {
                    expect(5) { attributes.getValue("id") { it.toLong() } }
                }
            }
            @Test fun `out of range`() {
                expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                    """<e id="5"/>""".konsumeXml().child("e") {
                        attributes.getValue("id") { it.toLong(10L..15) }
                    }
                }
            }
            @Test fun `out of range Opt`() {
                expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: Failed to convert the value '5' of attribute 'id': 'The number '5' is not in range 10..15") {
                    """<e id="5"/>""".konsumeXml().child("e") {
                        attributes.getValueOrNull("id") { it.toLong(10L..15) }
                    }
                }
            }
            @Test fun `failed type conversion provides location properly`() {
                expectThrows(KonsumerException::class, "line 1 column 14 at null null, in element <e>: Failed to convert the value 'foo' of attribute 'id': 'For input string: \"foo\"'") {
                    """<e id="foo"/>""".konsumeXml().child("e") {
                        attributes.getValue("id") { it.toLong() }
                    }
                }
            }
            @Test fun `returns null even if outside of range`() {
                """<e/>""".konsumeXml().child("e") {
                    expect(null) { attributes.getValueOrNull("id") { it.toLong(10L..15) } }
                }
            }
        }
        @Nested inner class `missing attribute` {
            @Test fun `return null when the attribute is not required`() {
                """<e/>""".konsumeXml().child("e") {
                    expect(null) { attributes.getValueOrNull("id") }
                    expect(null) { attributes.getValueIntOrNull("id") }
                }
            }
            @Test fun `fail when the attribute is required`() {
                expectThrows(KonsumerException::class, "line 1 column 5 at null null, in element <e>: Required attribute 'id' is missing. Available attributes: []") {
                    """<e/>""".konsumeXml().child("e") {
                        attributes.getValue("id")
                    }
                }
            }
            @Test fun `dump existing attributes`() {
                expectThrows(KonsumerException::class, "line 1 column 39 at null null, in element <e>: Required attribute 'id' is missing. Available attributes: [foo, {a}a:foo]") {
                    """<e foo="bar" xmlns:a="a" a:foo="baz"/>""".konsumeXml().child("e") {
                        attributes.getValue("id")
                    }
                }
            }
        }
        @Nested inner class unconsumed {
            @Test fun `by default unconsumed attributes are ignored`() {
                """<e id="5"/>""".konsumeXml().child("e") {}
            }
            @Test fun `fail on unconsumed attribute`() {
                expectThrows(KonsumerException::class, "line 1 column 12 at null null, in element <e>: The attribute id has not been consumed") {
                    """<e id="5"/>""".konsumeXml().apply {
                        settings.failOnUnconsumedAttributes = true
                        child("e") {}
                    }
                }
            }
            @Test fun `xml-lang`() {
                """<e xml:lang="en"/>""".konsumeXml().child("e") {
                    settings.failOnUnconsumedAttributes = true
                }
            }
        }
        @Test fun `attributes can only be consumed in the beginning`() {
            expectThrows(KonsumerException::class, "line 1 column 15 at null null, in element <e>: You can only read element attributes when the element has been freshly entered") {
                """<e id="5"><a/></e>""".konsumeXml().child("e") {
                    child("a") {}
                    attributes.getValue("id")
                }
            }
        }
        @Test fun `xml-lang`() {
            """<e xml:lang="en"/>""".konsumeXml().child("e") {
                settings.failOnUnconsumedAttributes = true
                expect("en") { attributes.getValue("lang", XMLConstants.XML_NS_URI) }
            }
        }
        // tests https://gitlab.com/mvysny/konsume-xml/-/issues/12
        @Test fun `atom-link href`() {
            lateinit var href: String
            val konsumer = """<rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0"><atom:link href="https://news.ycombinator.com/feed/" /></rss>""".konsumeXml()
            konsumer.child("rss") {
                child("link") {
                    href = attributes["href"]
                    expect("https://news.ycombinator.com/feed/") { href }
                }
            }
            expect("https://news.ycombinator.com/feed/") { href }
        }

        // tests https://gitlab.com/mvysny/konsume-xml/-/issues/12
        @Test fun `atom-link href 2`() {
            data class Feed(var name: String = "", var description: String = "", var url: String = "", var siteUrl: String = "")

            val feed = Feed()

            """<?xml version="1.0" encoding="UTF-8"?>
                <rss xmlns:atom="http://www.w3.org/2005/Atom" version="2.0">
                    <channel>
                        <atom:link href="https://news.ycombinator.com/feed/" />
                    </channel>
                </rss>
            """.konsumeXml().child("rss") {
                child("channel") {
                    allChildrenAutoIgnore(Names.of("title", "description", "link")) {
                        with(feed) {
                            when (tagName) {
                                "title" -> name = text()
                                "description" -> description = text()
                                "link" -> url = text()
                                "atom:link" -> siteUrl = attributes["href"]
                            }
                        }
                    }
                }
            }

            expect("https://news.ycombinator.com/feed/") { feed.siteUrl }
        }
}
