package com.gitlab.mvysny.konsumexml

import com.gitlab.mvysny.konsumexml.stax.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.expect

internal fun StaxReader.drain(): List<StaxEventType> = generateSequence { if (hasNext()) next() else null } .toList()

class KonsumerReaderTest {
    @Nested inner class JavaxXmlTests : AbstractKonsumerReaderTests(XmlParserType.JAVAX_XML)
    @Nested inner class ComBeaTests : AbstractKonsumerReaderTests(XmlParserType.COM_BEA)
    @Nested inner class XmlPullTests : AbstractKonsumerReaderTests(XmlParserType.XMLPULL)
}

abstract class AbstractKonsumerReaderTests(xmlParser: XmlParserType) : AbstractXmlParserTest(xmlParser) {
    @Test fun smoke() {
        val r: StaxReader = """<employee><id>1</id></employee>""".staxReader()
        expect(listOf(StaxEventType.StartElement, StaxEventType.StartElement, StaxEventType.Characters, StaxEventType.EndElement,
                StaxEventType.EndElement, StaxEventType.EndDocument)) { r.drain() }
    }

    @Test fun `push back`() {
        val r: StaxReader = """<employee><id>1</id></employee>""".staxReader()
        expect(StaxEventType.StartElement) { r.next() }
        expect(StaxEventType.StartElement) { r.next() }
        expect("id") { r.stax.elementName.toString() }
        r.pushBack()
        expect(StaxEventType.StartElement) { r.next() }
        expect("id") { r.stax.elementName.toString() }
        expect(StaxEventType.Characters) { r.next() }
        expect("1") { r.stax.text }
    }
}
