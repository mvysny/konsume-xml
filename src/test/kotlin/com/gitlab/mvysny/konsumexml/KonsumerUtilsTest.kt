package com.gitlab.mvysny.konsumexml

import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.expect

class KonsumerUtilsTest {
    @Nested inner class JavaxXmlTests : AbstractKonsumerUtilsTests(XmlParserType.JAVAX_XML)
    @Nested inner class ComBeaTests : AbstractKonsumerUtilsTests(XmlParserType.COM_BEA)
    @Nested inner class XmlPullTests : AbstractKonsumerUtilsTests(XmlParserType.XMLPULL)
}

abstract class AbstractKonsumerUtilsTests(xmlParserType: XmlParserType) : AbstractXmlParserTest(xmlParserType) {
        @Nested inner class allChildrenAutoIgnore {
            @Test fun simple() {
                val elements: MutableMap<String, String> = mutableMapOf()
                """
                <employee>
                    <id>1</id>
                    <name>Alba</name>
                    <salary>100</salary>
                </employee>
                """.konsumeXml().child("employee") {
                    allChildrenAutoIgnore(Names.of("id", "salary")) {
                        elements[localName] = text()
                    }
                }
                expect<Map<String, String>>(mapOf("id" to "1", "salary" to "100")) { elements }
            }
        }
        @Nested inner class textRecursively {
            @Test fun simple() {
                val text = """
                <employee>
                    <id>1</id>
                    <name>Alba</name>
                    <salary>100</salary>
                </employee>
                """.konsumeXml().textRecursively()
                expect(" 1 Alba 100 ") { text }
            }
            @Test fun `simple - no whitespaces`() {
                val text = """<employee><id>1</id><name>Alba</name><salary>100</salary></employee>""".konsumeXml().textRecursively()
                expect("1Alba100") { text }
            }
            @Test fun `element with text only`() {
                val text = """<p>hello!</p>""".konsumeXml().textRecursively()
                expect("hello!") { text }
            }
            @Test fun `comments are ignored automatically`() {
                val text = """<p>hello<!-- comment --> world</p>""".konsumeXml().textRecursively()
                expect("hello world") { text }
            }
            @Test fun `simple text`() {
                val text = """<p>hello <b>world</b> <i>again</i></p>""".trimMargin().konsumeXml().textRecursively()
                expect("hello world again") { text }
            }
            @Test fun `simple text 2`() {
                val text = """<p>hello <b>world </b><i>again</i></p>""".trimMargin().konsumeXml().textRecursively()
                expect("hello world again") { text }
            }
            @Nested inner class whitespaces {
                @Test fun collapse_no_trim() {
                    val text = """<p>
hello <b>world</b>   <i>again</i>
</p>""".konsumeXml().textRecursively()
                    expect(" hello world again ") { text }
                }
                @Test fun collapse() {
                    val text = """<p>
hello <b>world</b>   <i>again</i>
</p>""".konsumeXml().textRecursively(Whitespace.collapse)
                    expect("helloworldagain") { text }
                }
                @Test fun replace() {
                    val text = """<p>
hello <b>world</b>   <i>again</i>
</p>""".konsumeXml().textRecursively(Whitespace.replace)
                    expect(" hello world   again ") { text }
                }
                @Test fun preserve() {
                    val text = """<p>
hello <b>world</b>   <i>again</i>
</p>""".konsumeXml().textRecursively(Whitespace.preserve)
                    expect("\nhello world   again\n") { text }
                }
            }
        }
}
