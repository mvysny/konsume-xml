package com.gitlab.mvysny.konsumexml.stax

import org.junit.jupiter.api.Test
import kotlin.test.expect

class StaxParserFactoryTest {
    @Test fun `When running tests on JDK, all parsers are available`() {
        expect(true) { StaxParserFactory.isJavaxXmlStreamAvailable }
        expect(true) { StaxParserFactory.isOrgXmlpullAvailable }
    }
}
