package readmeexamples

import com.gitlab.mvysny.konsumexml.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import java.io.File
import java.math.BigDecimal
import kotlin.test.expect

data class TaskSchedule(val buildingId: Int, val roomId: Int, val buildingName: String, val roomName: String,
                        val managementAreaName: String) {
    companion object {
        fun xml(k: Konsumer): TaskSchedule {
            k.checkCurrent("TaskShedule")
            return TaskSchedule(k.childInt("building_id"),
                    k.childInt("room_id"),
                    k.childText("building_name"),
                    k.childText("room_name"),
                    k.childText("management_area_name"))
        }
    }
}

class StackOverflowExampleTest {
    @Nested
    inner class JavaxXmlTests : AbstractReadmeExamplesTests(XmlParserType.JAVAX_XML)
    @Nested
    inner class ComBeaTests : AbstractReadmeExamplesTests(XmlParserType.COM_BEA)
    @Nested
    inner class XmlPullTests : AbstractReadmeExamplesTests(XmlParserType.XMLPULL)
}

abstract class AbstractStackOverflowExampleTests(xmlParserType: XmlParserType) : AbstractXmlParserTest(xmlParserType) {
        @Test fun test() {
            val taskScheduleList = mutableListOf<TaskSchedule>()
            File("src/test/files/example.xml").konsumeXml().use { k ->
                k.child("Response") {
                    childText("ReturnCode") // skip over <ReturnCode>
                    childText("Message") // skip over <Message>
                    child("Result") {
                        child("Task") {
                            child("TaskShedule") {
                                taskScheduleList.add(TaskSchedule.xml(this))
                            }
                        }
                    }
                }
            }
            println(taskScheduleList)
            expect(1) { taskScheduleList.size }
        }

        @Test fun test2() {
            val shipment = """
            <Shipment>
                <sUID>yPkmfG3caT6cxexj5oWy34WiUUjj8WliWit45IzFVSOt6gymAOUA==</sUID>
                <Shipping>0.00</Shipping>
                <DocType>SO</DocType>
            </Shipment>
            """.trimIndent().konsumeXml().child("Shipment") { Shipment.xml(this) }
            println(shipment)
            expect(Shipment("yPkmfG3caT6cxexj5oWy34WiUUjj8WliWit45IzFVSOt6gymAOUA==", BigDecimal("0.00"), "SO")) { shipment }
        }

        @Test fun test3() {
            val file = File("src/test/files/person.xml")
            file.konsumeXml().use { k ->
                k.child("Person") {
                    println(childText("Name"))
                    println(childText("Gender"))
                    println(childText("More"))
                }
            }
        }

        @Test fun test4() {
            val konsumer = """
            <resources>
                <string name="name1">content1</string>
                <string name="name2">content2</string>
                <string name="name3">content3</string>
            </resources>
            """.trimIndent().konsumeXml()
            val resources: List<Resource> = konsumer.child("resources") {
                children("string") {
                    Resource(attributes["name"], text())
                }
            }
            println(resources)
            expect(3) { resources.size }
        }

        @Test fun test5() {
            val file = File("src/test/files/rootelement.xml")
            file.konsumeXml().use { k ->
                k.child("rootelement1") {
                    child("subelement") {
                        val item = attributes.getValueOrNull("item")
                        val e = childText("element")
                        println(item); println(e)
                    }
                    child("subelement") {
                        println(childText("element"))
                        println(childText("subsubelement"))
                    }
                }
            }
        }

        @Test fun test6() {
            expect("Hello World!") { "<foo>Hello World!</foo>".konsumeXml().childText("foo") }
        }
}

data class Shipment(val uid: String, val shipping: BigDecimal, val docType: String) {
    companion object {
        fun xml(k: Konsumer): Shipment {
            k.checkCurrent("Shipment")
            return Shipment(k.childText("sUID"),
                    k.childText("Shipping") { it.toBigDecimal() },
                    k.childText("DocType"))
        }
    }
}

data class Resource(val name: String, val content: String) {
    companion object {
        fun xml(k: Konsumer): Resource {
            k.checkCurrent("string")
            return Resource(k.attributes["name"], k.text())
        }
    }
}
