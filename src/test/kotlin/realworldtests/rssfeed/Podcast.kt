package realworldtests.rssfeed

import com.gitlab.mvysny.konsumexml.*
import java.io.File

data class Podcast(
        val title: String,
        val link: String?,
        val description: String?,
        val image: String?,
        val episodeList: List<PodcastEpisode>
) {
    companion object {
        fun xml(k: Konsumer): Podcast {
            k.checkCurrent("channel")
            var title = ""
            var link = ""
            var description = ""
            var image = ""
            val episodeList = mutableListOf<PodcastEpisode>()
            k.allChildrenAutoIgnore(Names.of("title", "description", "link", "image", "item")) {
                when (localName) {
                    "title" -> title = text()
                    "link" -> link = text()
                    "description" -> description = text()
                    "image" -> if (!name.hasNamespace) {
                        skipContents()
                    } else {
                        image = attributes["href"]
                    }
                    "item" -> episodeList += PodcastEpisode.xml(this)
                }
            }
            return Podcast(title, link, description, image, episodeList)
        }

        fun file(): Podcast {
            val file = File("src/test/files/rss_feed.xml")
            return file.konsumeXml().use { k ->
                k.child("rss") {
                    child("channel") { Podcast.xml(this) }
                }
            }
        }
    }
}

data class PodcastEpisode(
        val title: String,
        val link: String,
        val description: String,
        val image: String,
        val trackURL: String) {
    companion object {
        fun xml(k: Konsumer): PodcastEpisode {
            k.checkCurrent("item")
            var title = ""
            var link = ""
            var description = ""
            var image = ""
            var trackURL = ""
            k.allChildrenAutoIgnore(Names.of("title", "description", "link", "enclosure", "image")) {
                when (localName) {
                    "title" -> if (!name.hasNamespace) {
                        skipContents()
                    } else {
                        title = text()
                    }
                    "link" -> link = text()
                    "description" -> description = text()
                    "image" -> if (!name.hasNamespace) {
                        skipContents()
                    } else {
                        image = attributes["href"]
                    }
                    "enclosure" -> trackURL = attributes["url"]
                }
            }
            return PodcastEpisode(title, link, description, image, trackURL)
        }
    }
}