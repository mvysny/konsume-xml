package realworldtests.rssfeed

import com.gitlab.mvysny.konsumexml.AbstractXmlParserTest
import com.gitlab.mvysny.konsumexml.XmlParserType
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import kotlin.test.expect

class PodcastTest {
    @Nested inner class JavaxXmlPodcastTests : AbstractPodcastTests(XmlParserType.JAVAX_XML)
    @Nested inner class ComBeaPodcastTests : AbstractPodcastTests(XmlParserType.COM_BEA)
    @Nested inner class XmlPullPodcastTests : AbstractPodcastTests(XmlParserType.XMLPULL)
}

abstract class AbstractPodcastTests(xmlParser: XmlParserType) : AbstractXmlParserTest(xmlParser) {
    @Test fun smoke() {
        val podcast = Podcast.file()
        expect("Glasgow & Barcelona based podcast, hosted by Sean McDonald. Contact at seanmcdonald.podcast@gmail.com.") { podcast.description }
        expect("https://storage.buzzsprout.com/variants/SZw5nedbLov7HWYccx6deFoK/8d66eb17bb7d02ca4856ab443a78f2148cafbb129f58a3c81282007c6fe24ff2?.jpg") { podcast.image }
        expect("") { podcast.link }
        expect("Blethered") { podcast.title }
        expect(23) { podcast.episodeList.size }
        expect("Episode 21 - The Impact of Addiction // with Shaun Toner") { podcast.episodeList.first().title }
        expect("We talk about Si’s early days at Celtic, working under Tommy Burns in the youth academy, and involvement under Gordon Strachan.") { podcast.episodeList[1].description }
        expect("https://www.buzzsprout.com/243355/1103675") { podcast.episodeList[2].link }
        expect("https://www.buzzsprout.com/243355/1103120-episode-18-singer-songwriter-beerjacket.mp3") { podcast.episodeList[3].trackURL }
    }
}
