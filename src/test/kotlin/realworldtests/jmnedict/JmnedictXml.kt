package realworldtests.jmnedict

import com.gitlab.mvysny.konsumexml.Konsumer
import com.gitlab.mvysny.konsumexml.konsumeXml
import java.io.File
import javax.xml.XMLConstants

data class JMnedict(val entry: List<Entry>) {
    companion object {
        fun xml(e: Konsumer): JMnedict {
            e.checkCurrent("JMnedict")
            return JMnedict(e.children("entry") { Entry.xml(this) })
        }

        fun file(): JMnedict {
            val file = File("src/test/files/JMnedict_part.xml")
            return file.konsumeXml().use { k ->
                k.settings.failOnUnconsumedAttributes = true
                k.child("JMnedict") { xml(this) }
            }
        }
    }
}

/**
 * Entries consist of kanji elements, reading elements
name translation elements. Each entry must have at
least one reading element and one sense element. Others are optional.
 * @property entSeq A unique numeric sequence number for each entry
 */
data class Entry(val entSeq: String, val kEle: List<KEle>, val rEle: List<REle>, val trans: List<Trans>) {
    init {
        require(rEle.isNotEmpty())
        require(trans.isNotEmpty())
    }

    companion object {
        fun xml(e: Konsumer): Entry {
            e.checkCurrent("entry")
            return Entry(
                    e.childText("ent_seq"),
                    e.children("k_ele") { KEle.xml(this) },
                    e.children("r_ele", 1) { REle.xml(this) },
                    e.children("trans", 1) { Trans.xml(this) }
            )
        }
    }
}

/**
 * The kanji element, or in its absence, the reading element, is
the defining component of each entry.
The overwhelming majority of entries will have a single kanji
element associated with an entity name in Japanese. Where there are
multiple kanji elements within an entry, they will be orthographical
variants of the same word, either using variations in okurigana, or
alternative and equivalent kanji. Common "mis-spellings" may be
included, provided they are associated with appropriate information
fields. Synonyms are not included; they may be indicated in the
cross-reference field associated with the sense element.
 * @property keb This element will contain an entity name in Japanese
which is written using at least one non-kana character (usually
kanji, but can be other characters). The valid
characters are kanji, kana, related characters such as chouon and
kurikaeshi, and in exceptional cases, letters from other alphabets.
 * @property keInf This is a coded information field related specifically to the
orthography of the keb, and will typically indicate some unusual
aspect, such as okurigana irregularity.
 * @property kePri This and the equivalent re_pri field are provided to record
information about the relative priority of the entry, and are for
use either by applications which want to concentrate on entries of
a particular priority, or to generate subset files. The reason
both the kanji and reading elements are tagged is because on
occasions a priority is only associated with a particular
kanji/reading pair.
 */
data class KEle(val keb: String, val keInf: List<String>, val kePri: List<String>) {
    companion object {
        fun xml(e: Konsumer): KEle {
            e.checkCurrent("k_ele")
            return KEle(e.childText("keb"),
                    e.childrenText("ke_inf"),
                    e.childrenText("ke_pri")
            )
        }
    }
}

/**
 * The reading element typically contains the valid readings
of the word(s) in the kanji element using modern kanadzukai.
Where there are multiple reading elements, they will typically be
alternative readings of the kanji element. In the absence of a
kanji element, i.e. in the case of a word or phrase written
entirely in kana, these elements will define the entry.
 * @property reb this element content is restricted to kana and related
characters such as chouon and kurikaeshi. Kana usage will be
consistent between the keb and reb elements; e.g. if the keb
contains katakana, so too will the reb.
 * @property reRestr This element is used to indicate when the reading only applies
to a subset of the keb elements in the entry. In its absence, all
readings apply to all kanji elements. The contents of this element
must exactly match those of one of the keb elements.
 * @property reInf General coded information pertaining to the specific reading.
Typically it will be used to indicate some unusual aspect of
the reading.
 * @property rePri See the comment on ke_pri above.
 */
data class REle(val reb: String, val reRestr: List<String>, val reInf: List<String>, val rePri: List<String>) {
    companion object {
        fun xml(e: Konsumer): REle {
            e.checkCurrent("r_ele")
            return REle(e.childText("reb"),
                    e.childrenText("re_restr"),
                    e.childrenText("re_inf"),
                    e.childrenText("re_pri")
            )
        }
    }
}

/**
 * The trans element will record the translational equivalent
of the Japanese name, plus other related information.
 * @property nameType The type of name, recorded in the appropriate entity codes.
 * @property xref This element is used to indicate a cross-reference to another
entry with a similar or related meaning or sense. The content of
this element is typically a keb or reb element in another entry. In some
cases a keb will be followed by a reb and/or a sense number to provide
a precise target for the cross-reference. Where this happens, a JIS
"centre-dot" (0x2126) is placed between the components of the
cross-reference.
 */
data class Trans(val nameType: List<String>, val xref: List<String>, val transDet: List<TransDet>) {
    companion object {
        fun xml(e: Konsumer): Trans {
            e.checkCurrent("trans")
            return Trans(
                    e.childrenText("name_type"),
                    e.childrenText("xref"),
                    e.children("trans_det") { TransDet.xml(this) }
            )
        }
    }
}

/**
 * The actual translations of the name, usually as a transcription
into the target language.
 * @property lang The xml:lang attribute defines the target language of the
translated name. It will be coded using the three-letter language
code from the ISO 639-2 standard. When absent, the value "eng"
(i.e. English) is the default value. The bibliographic (B) codes
are used.
 */
data class TransDet(val content: String, val lang: String?) {
    companion object {
        fun xml(e: Konsumer): TransDet {
            e.checkCurrent("trans_det")
            val lang = e.attributes.getValueOrNull("lang", XMLConstants.XML_NS_URI)
            return TransDet(e.text(), lang)
        }
    }
}
