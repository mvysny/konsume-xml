package realworldtests.jmnedict

import com.gitlab.mvysny.konsumexml.AbstractXmlParserTest
import com.gitlab.mvysny.konsumexml.XmlParserType
import org.junit.jupiter.api.Test
import kotlin.test.expect

class JmnedictXmlTest : AbstractXmlParserTest(XmlParserType.JAVAX_XML) {
    @Test fun smoke() {
        val dict = JMnedict.file()
        expect(234) { dict.entry.size }
        expect("foo") { dict.entry[0].trans[0].transDet[0].lang }
    }
}
