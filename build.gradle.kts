import org.gradle.api.tasks.testing.logging.TestExceptionFormat
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "2.1.10"
    `maven-publish`
    signing
}

defaultTasks("clean", "build")

group = "com.gitlab.mvysny.konsume-xml"
version = "1.3-SNAPSHOT"

repositories {
    mavenCentral()
}

// Kotlin 1.5.0 deprecates 1.6 target; also new Android SDKs are able to process Java 8 bytecode.
// Let's not support Java 7 as well and let's start testing on Java 8+
tasks.withType<KotlinCompile> {
    compilerOptions.jvmTarget = JvmTarget.JVM_1_8  // retain compatibility with Android
}

kotlin {
    explicitApi()
}

dependencies {
    // to help parse XML date time types
    implementation("javax.xml.bind:jaxb-api:2.3.1")
    // Provides XML Pull Parser API which is built-in in Android but not present in JavaSE.
    compileOnly("org.ogce:xpp3:1.1.6") {
        exclude(group = "junit")
    }

    // tests
    testImplementation(kotlin("stdlib-jdk8")) // forces correct Kotlin version
    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.11.0")
    testImplementation("org.ogce:xpp3:1.1.6") {
        exclude(group = "junit")
    }
    // also test with the `stax` library. See https://gitlab.com/mvysny/konsume-xml/-/issues/12 for more details.
    testImplementation("stax:stax:1.2.0")
    testRuntimeOnly("org.junit.platform:junit-platform-launcher")
}

// following https://dev.to/kengotoda/deploying-to-ossrh-with-gradle-in-2020-1lhi
java {
    withJavadocJar()
    withSourcesJar()
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

tasks.withType<Javadoc> {
    isFailOnError = false
}

publishing {
    repositories {
        maven {
            setUrl("https://oss.sonatype.org/service/local/staging/deploy/maven2/")
            credentials {
                username = project.properties["ossrhUsername"] as String? ?: "Unknown user"
                password = project.properties["ossrhPassword"] as String? ?: "Unknown user"
            }
        }
    }
    publications {
        create("mavenJava", MavenPublication::class.java).apply {
            groupId = project.group.toString()
            this.artifactId = "konsume-xml"
            version = project.version.toString()
            pom {
                description = "Konsume-XML: A simple functional XML parser with no annotations"
                name = "Konsume-XML"
                url = "https://gitlab.com/mvysny/konsume-xml"
                licenses {
                    license {
                        name = "The MIT License"
                        url = "https://opensource.org/licenses/MIT"
                        distribution = "repo"
                    }
                }
                developers {
                    developer {
                        id = "mavi"
                        name = "Martin Vysny"
                        email = "martin@vysny.me"
                    }
                }
                scm {
                    url = "https://gitlab.com/mvysny/konsume-xml"
                }
            }
            from(components["java"])
        }
    }
}

signing {
    sign(publishing.publications["mavenJava"])
}

tasks.withType<Test> {
    useJUnitPlatform()
    testLogging {
        // to see the stacktraces of failed tests in the CI console.
        exceptionFormat = TestExceptionFormat.FULL
    }
}

if (JavaVersion.current() > JavaVersion.VERSION_11 && gradle.startParameter.taskNames.contains("publish")) {
    throw GradleException("Release this library with JDK 11 or lower, to ensure JDK8 compatibility; current JDK is ${JavaVersion.current()}")
}
