# Konsume-XML - XML parser for Kotlin that doesn't sax ;)

[![pipeline status](https://gitlab.com/mvysny/konsume-xml/badges/master/pipeline.svg)](https://gitlab.com/mvysny/konsume-xml/commits/master)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/com.gitlab.mvysny.konsume-xml/konsume-xml/badge.svg)](https://maven-badges.herokuapp.com/maven-central/com.gitlab.mvysny.konsume-xml/konsume-xml)

The simplest and most powerful XML parsing library for Android and JVM. Requires Kotlin. Does not use any annotations, instead
focuses on functional approach. Example:

```kotlin
"""
<employee>
    <id>1</id>
    <name>Alba</name>
    <salary>100</salary>
</employee>
""".konsumeXml().apply {
    child("employee") { 
        println(childInt("id"))
        println(childText("name"))
        println(childInt("salary"))
    }
}
```

The above will print:
```
1
Alba
100
```

Now go and compare that code to the [official ridiculous way of parsing XML data in Android](https://developer.android.com/training/basics/network-ops/xml).

The library allows you to write a DSL that consumes (rather than generates) a XML. The library polls given XML via Pull/STaX (the `javax.xml.stream` API built-in in JDK, and the `org.xmlpull.v1` API built-in in Android)
which is very memory-efficient. By calling methods like `child()` or `text()` you declare what content you expect; if
the next content is as you expected it is parsed, otherwise an exception is thrown.

This allows you to define expected content of an XML in a DSL tree-like way; the library also performs
validation on-the-fly - any unexpected element, attribute or inconvertible text contents
are reported immediately via `KonsumerException` with precise location and error message.

The library allows for easy object binding. For example, the example above can be rewritten as:

```kotlin
data class Employee(val id: Int, val name: String, val salary: Int) {
    companion object {
        fun xml(k: Konsumer): Employee {
            k.checkCurrent("employee")
            return Employee(k.childInt("id"), k.childText("name"), k.childInt("salary"))
        }
    }
}

val employee: Employee = """
    <employee>
        <id>1</id>
        <name>Alba</name>
        <salary>100</salary>
    </employee>
    """.konsumeXml().child("employee") { Employee.xml(this) }
println(employee)
```

will print `Employee(id=1, name=Alba, salary=100)`.

> If you like the library, please star it. The more stars, the more popularity and more maintainenance the library will receive.

*Quick note:* the order of elements matters. konsume-xml doesn't perform any look-ahead; you have
to consume the elements in the exact order in which they appear in the XML file. This way,
you're also performing the XML validation. If you'd like to parse XML elements out-of-order,
please see below on how to parse interleaved elements.

## Compatibility

Compatibility Matrix:

| konsume-xml version | Min Java/Kotlin | Supported Android version |
|---------------------|-----------------|---------------------------|
| 0.14 and lower      | 1.7+/1.4+       | Android API 8+ (Android 2.2.x). |
| 1.0+ (recommended)  | 1.8+/1.5+       | Not sure... needs to handle JVM 8 bytecode transformed to DEX |

The library heavily uses the DSL language feature of Kotlin, therefore it makes no sense to access Konsume-XML's API
directly from Java. Luckily, it's easy to mix Kotlin and Java code in your project. You can therefore write the classes
and parsing code as a Kotlin function, then call the function from Java.

## Adding To Your Project

Konsume-XML is in Maven Central. Adding it to your project is as easy as adding the following into your Gradle script:

```groovy
repositories {
    mavenCentral()
}
dependencies {
    implementation("com.gitlab.mvysny.konsume-xml:konsume-xml:x.y")
}
```

> Please see the latest version of konsume-xml by looking at the `maven central` tag at the top of this page.

## Validation

The idea is that by reading the values of particular elements and/or particular attributes you express
that you expect such nodes to be present in the XML. If they're missing from the XML then
it's a validation error and the parsing fails.

All other elements and attributes are *not expected*
to be present in the XML - it is a validation error if an unknown element (and optionally an unknown attribute)
is present in the XML.

Therefore, simply by calling `child()`/`childText()` functions you effectively validate the
contents of the XML. No schema is therefore needed!

## No Annotations

There are NO annotations in this library. Seriously. At first I tried to use JAXB
but I was drowning in libraries and annotations until I figured out how to generate
classes from the XML Schema. Then, the generator stopped working on JDK 11 since JAXB
is no longer packaged in JVM.

Then I tried [Jackson XML](https://github.com/FasterXML/jackson-dataformat-xml) but the documentation was somewhat lacking
and I failed to understand how I should parse repeated elements like `<entry key="foo">bar</entry>`.
Not to mention that the Jackson XML, Jackson Core and Woodstox are simply HUGE 1-2MB libraries with lots of code -
if something fails there is no way I can dig through that code and understand what's going on. Compared to that,
konsume-xml is tiny: it's a 90kb jar file with no further dependencies - it uses the XML parser
provided by JVM or Android.

Then I tried [Simple XML](http://simple.sourceforge.net/download/stream/doc/tutorial/tutorial.php). It's annotation-based,
so I've tried lots of magic annotation combinations to make something happen. The library frequently failed with cryptic error messages,
finally it simply failed with an error `duplicite key 'object'` or something like that and I couldn't figure out what's wrong.

Then I tried [XStream](http://x-stream.github.io/converter-tutorial.html) which actually looks
really good since there are no annotations; however it's still based on the idea that you
pre-configure it with a load of settings and converters and then you unleash it on POJOs and
hope for the best. I prefer just reading the XML with my code, validating and converting
on-the-go with my custom in-place code.

Instead of pre-configuring the XML binding library with magic combinations of settings, you simply call high-level parsing
functions at any time you need, having the power of an actual programming language at your disposal instead of
an [embedded weak mini-language](https://blog.softwaremill.com/the-case-against-annotations-4b2fb170ed67). Let's move
to the post-annotation programming, shall we?

## More Examples

Reading a simple XML is very easy. There are no annotations to add, you simply go and take
advantage of the `Konsumer` class API. For example, to read the following XML into an `Example` class:

```xml
<example index="123">
   <text>Example message</text>
</example>
```

You can use the following code:

```kotlin
data class Example(val index: Int, val text: String) {
    companion object {
        fun xml(k: Konsumer): Example {
            k.checkCurrent("example")
            return Example(k.attributes.getValueInt("index"), k.childText("text"))
        }
    }
}

val example: Example = File("example.xml").konsumeXml().use { k -> k.child("example") { Example.xml(this) } }
```

### Nested Object Deserialization

Nested object deserialization is of course also possible. This is where a serializable object can contain
any number of serializable objects, to any depth. Take the example shown in the code
snippet below. This shows several objects that are linked together to form a single serializable
entity. Here the root configuration object contains a server object, which in turn contains a
security information object. 

```xml
<configuration id="1234">
   <server port="80">
      <host>www.domain.com</host>
      <security ssl="true">
         <keyStore>example keystore</keyStore>
      </security>
   </server>
</configuration>
```

The following code can be used to read the XML above:

```kotlin
data class Security(val ssl: Boolean, val keyStore: String) {
    companion object {
        fun xml(k: Konsumer): Security {
            k.checkCurrent("security")
            return Security(k.attributes.getValue("ssl") { it.xmlBoolean() }, k.childText("keyStore"))
        }
    }
}
data class Server(val port: Int, val host: String, val security: Security) {
    companion object {
        fun xml(k: Konsumer): Server {
            k.checkCurrent("server")
            return Server(k.attributes.getValueInt("port"),
                    k.childText("host"),
                    k.child("security") { Security.xml(this) })
        }
    }
}
data class Configuration(val server: Server, val id: Int) {
    companion object {
        fun xml(k: Konsumer): Configuration {
            k.checkCurrent("configuration")
            // the attributes are only accessible straight after entering an element. therefore, we need to
            // read attributes as the first thing, before digging into element contents
            val id = k.attributes.getValueInt("id")
            return Configuration(k.child("server") { Server.xml(this) }, id)
        }
    }
}

val cfg = File("cfg.xml").konsumeXml().use { k -> k.child("configuration") { Configuration.xml(this) } }
```

### Optional elements and attributes

Methods like `child()`, `childText()` and `attributes.getValue()` will fail with `KonsumerException`
if the element or attribute is not present. However, often it's desired to have optional elements
and/or attributes. For such cases make sure to call the functions ending with `OrNull`, such as
`childOrNull()`, `childTextOrNull()` and `attributes.getValueOrNull()`.

For example:
```xml
<optionalExample id="10">
   <address>Some example address</address>
</optionalExample>
```

```kotlin
data class OptionalExample(val version: Int?, val id: String, val name: String?, val address: String) {
    companion object {
        fun xml(k: Konsumer): OptionalExample {
            k.checkCurrent("optionalExample")
            return OptionalExample(k.attributes.getValueIntOrNull("version"),
                    k.attributes.getValue("id"),
                    k.childTextOrNull("name"),
                    k.childText("address"))
        }
    }
}

val e = File("example.xml").konsumeXml().use { k -> k.child("optionalExample") { OptionalExample.xml(this) } }
```

This will produce `OptionalExample(version=null, id=10, name=null, address=Some example address)`.

### Reading a list of elements

In XML configuration and in Java objects there is often a one-to-many relationship from a parent to a child object. In order
to support this common relationship the `children()` function has been provided. Take the example shown below.  

```xml
<propertyList name="example">
   <list>
      <entry key="one">
         <value>first value</value>
      </entry>
      <entry key="two">
         <value>first value</value>
      </entry>
      <entry key="three">
         <value>first value</value>
      </entry>
      <entry key="four">
         <value>first value</value>
      </entry>
   </list>
</propertyList>
```

The appropriate code to read this XML would be:

```kotlin
data class PropertyList(val name: String, val list: List<Entry>) {
    companion object {
        fun xml(k: Konsumer): PropertyList {
            k.checkCurrent("propertyList")
            return PropertyList(k.attributes["name"], k.child("list") { children("entry") { Entry.xml(this) } })
        }
    }
}

data class Entry(val key: String, val value: String) {
    companion object {
        fun xml(k: Konsumer): Entry {
            k.checkCurrent("entry")
            return Entry(k.attributes["key"], k.childText("value"))
        }
    }
}

val list = File("list.xml").konsumeXml().use { k -> k.child("propertyList") { PropertyList.xml(this) } }
```

### Dealing with an inline list of elements

When dealing with third party XML or with XML that contains a grouping of related elements a common format involves the elements to exist in a sequence with no wrapping parent element.
It's easy to accommodate such XMLs:

```xml
<propertyList>
   <name>example</name>
   <entry key="one">
      <value>first value</value>
   </entry>
   <entry key="two">
      <value>second value</value>
   </entry>
   <entry key="three">
      <value>third value</value>
   </entry>
</propertyList>
```

```kotlin
data class PropertyList(val name: String, val list: List<Entry>) {
    companion object {
        fun xml(k: Konsumer): PropertyList {
            k.checkCurrent("propertyList")
            return PropertyList(k.attributes["name"], k.child("list") { children("entry") { Entry.xml(this) } })
        }
        fun inlineXml(k: Konsumer): PropertyList {
            k.checkCurrent("propertyList")
            return PropertyList(k.childText("name"), k.children("entry") { Entry.xml(this) })
        }
    }
}

data class Entry(val key: String, val value: String) {
    companion object {
        fun xml(k: Konsumer): Entry {
            k.checkCurrent("entry")
            return Entry(k.attributes["key"], k.childText("value"))
        }
    }
}

val list = File("list.xml").konsumeXml().use { k -> k.child("propertyList") { PropertyList.inlineXml(this) } }
```

### Scattering inline element entries (interleaved element support)

Elements that are scattered throughout an XML document are also supported:

```xml
<fileSet path="/user/niall">
   <include pattern=".*.jar"/>
   <exclude pattern=".*.bak"/>
   <exclude pattern="~.*"/>
   <include pattern=".*.class"/>
   <exclude pattern="images/.*"/>
</fileSet>
``` 

This XML can be read using the `children()` function that looks up a set of elements:

```kotlin
data class FileSet(val path: String, val include: Set<String>, val exclude: Set<String>) {
    companion object {
        fun xml(k: Konsumer): FileSet {
            k.checkCurrent("fileSet")
            val path = k.attributes["path"]
            val include = mutableSetOf<String>()
            val exclude = mutableSetOf<String>()
            k.children(Names.of("include", "exclude")) {
                val pattern = attributes["pattern"]
                (if (name!!.localPart == "include") include else exclude).add(pattern)
            }
            return FileSet(path, include, exclude)
        }
    }
}

val list = File("fileset.xml").konsumeXml().use { k -> k.child("fileSet") { FileSet.xml(this) } }
println(list)
```

This will print `FileSet(path=/user/niall, include=[.*.jar, .*.class], exclude=[.*.bak, ~.*, images/.*])`.

### Using XML namespaces

Namespaces are used to qualify an element or an attribute in an XML document. To read elements,
Konsumer uses the simplest algorithm possible and it matches local names only - it ignores namespace completely.
However, for attributes you need to specify both local name and namespace. For example:

```xml
<parent xmlns="http://domain/parent">
   <pre:child xmlns:pre="http://domain/child">
      <name>John Doe</name>
      <address xmlns="">
          <street>Sin City</street>
      </address>
   </pre:child>
</parent>
```

Use the following code to read this snippet:

```kotlin
data class Parent(val child: Child) {
    companion object {
        fun xml(k: Konsumer): Parent {
            k.checkCurrent("parent")
            return Parent(k.child("child") { Child.xml(this) })
        }
    }
}

data class Child(val name: String, val street: String) {
    companion object {
        fun xml(k: Konsumer): Child {
            k.checkCurrent("child")
            return Child(k.childText("name"), k.child("address") { childText("street") } )
        }
    }
}

val parent = File("parent.xml").konsumeXml().use { k -> k.child("parent") { Parent.xml(this) } }
```

### Data Type Conversion

All of XML data types are supported:

```xml
<dateList created="2002-05-30T09:30:10.5">
    <date>2002-05-30T09:00:00Z</date>
    <date>2002-05-30T09:30:10+06:00</date>
</dateList>
```

Parse the above XML with this code:

```kotlin
data class DateList(val created: Date, val dates: List<Date>) {
    companion object {
        fun xml(k: Konsumer): DateList {
            k.checkCurrent("dateList")
            return DateList(k.attributes.getValue("created") { it.xmlDateTime().time },
                    k.childrenText("date") { it.xmlDateTime().time }
                )
        }
    }
}

val dateList = File("datelist.xml").konsumeXml().use { k -> k.child("dateList") { DateList.xml(this) } }
println(dateList)
```

This will print:

```
DateList(created=Thu May 30 09:30:10 EEST 2002, dates=[Thu May 30 12:00:00 EEST 2002, Thu May 30 06:30:10 EEST 2002])
```

Konsume-XML defines String extension functions which parse various XML data types:

* `String.xmlDateTime()` parses [XML Date Time format](https://www.w3schools.com/xml/schema_dtypes_date.asp) strings such as `2002-05-30T09:00:00`, `2002-05-30T09:30:10.5`, `2002-05-30T09:30:10Z` or `2002-05-30T09:30:10-06:00`
  and produces a `Calendar` (can't use `LocalDateTime` since it's not present in Android)
* `String.xmlDate()` Parses the [XML Date format](https://www.w3schools.com/xml/schema_dtypes_date.asp), for example `2002-09-24`.
* `String.xmlTime()` Parses the [XML Time format](https://www.w3schools.com/xml/schema_dtypes_date.asp), for example `09:00:00`, `09:30:10.5`, `09:30:10Z` or `09:30:10-06:00`.
* `String.xmlBoolean()` parses `"true"`, `"false"`, `"0"` or `"1"`, case-insensitive to a boolean type.
  Fails for any other String value.
* `String.xmlDecimal()` parses the XML decimal format to `BigDecimal`
* `String.xmlFloat()` parses the XML float to `Float`, handling `-INF` correctly.
* `String.xmlDouble()` parses the XML double to `Double`, handling `-INF` correctly.
* `String.xmlHexBinary()` parses the `xsd:hexBinary` data type.
* `String.xmlBase64Binary()` parses `xsd:base64Binary`.

In order to parse Long, simply convert the String to Long using Kotlin built-in conversion function:
* `k.attributes.getValue("created") { it.toLong() }`
* `k.attributes.getValue("created") { it.toLong(0L..1000L) }` also validates that the long value is in given range.

There is built-in support for parsing Int:
* `k.attributes.getValueInt("created")`

### Whitespaces

Since konsume-xml 0.9, every Konsumer method dealing with text accepts an optional parameter named `whitespace`.
The parameter defaults to `Whitespace.collapse`. The possible values are:

* collapse (default): This will collapse all white space characters, and remove leading/trailing whitespaces:
  line feeds, tabs, spaces, carriage returns are replaced with spaces,
  multiple whitespace characters are reduced to a single space, and
  leading and trailing whitespaces are removed completely.
* collapse_no_trim (since konsume-xml 0.12): This will collapse all white space characters:
  line feeds, tabs, spaces, carriage returns are replaced with spaces,
  multiple whitespace characters are reduced to a single space.
  Leading and trailing whitespaces are however not removed as opposed to `collapse`.
  This is useful when parsing mixed contents. See the `textRecursively()` function for more details.
* preserve: Preserves all whitespaces - will not remove any whitespaces.
* replace: This will replace all white space characters (line feeds, tabs, spaces, and carriage returns) with spaces,
  however it will not collapse consecutive whitespaces.

To specify the whitespace processing different than the default `collapse`, just specify it
as a parameter, for example:

```kotlin
"""<e>&#x9;foo&#x9;&#x9;</e>""".konsumeXml().childText("e", Whitespace.replace)
```
will return `" foo  "`.

### Mixed Contents

Since konsume-xml 0.9 it is possible to read mixed contents, such as `<p>a <b>bold</b> text with <i>italic</i> contents</p>`.
For example, this code will transform such a XML to Markdown:

```kotlin
fun Konsumer.toMarkdown(): String = buildString {
    while (!isFinished) {
        // read some text until a child element is encountered
        append(text(Whitespace.replace, failOnElement = false))
        // if there is no more content, bail out
        if (isFinished) { break }
        // an element has been encountered, read it. Don't read more than 1 element though,
        // since that would automatically also read the text between the elements.
        child(Names.of("b", "i", "p")) {
            // reading the contents of the child element
            val markdown = when (localName) {
                "b" -> "*${text()}*"
                "i" -> "**${text()}**"
                else -> toMarkdown()
            }
            append(markdown)
        }
    }
}

val xml = "<p>a <b>bold</b> text with <i>italic</i> contents</p>"
println(xml.konsumeXml().child("p") { toMarkdown() }) 
```

The above code will print `a *bold* text with **italic** contents`.

### Recursive Text Contents

Since konsume-xml 0.12 it's possible to read the text contents recursively, by
using the `textRecursively()` function. Examples of whitespace handling for the
example XML input:

```xml
<p>
hello <b>world</b>   <i>again</i>
</p>
```

* `konsumeXml().textRecursively()` will return `" hello world again "`
* `konsumeXml().textRecursively(Whitespace.collapse)` will return `"helloworldagain"`
* `konsumeXml().textRecursively(Whitespace.replace)` will return `" hello world   again "`
* `konsumeXml().textRecursively(Whitespace.preserve)` will return `"\nhello world   again\n"`

To read a text paragraph with formatting elements, it's therefore best to call `textRecursively().trim()`.

### Streaming

Often the root element is just a list, containing lots of small-ish child elements. You can read those elements individually,
but loading the whole XML into memory would take too much memory. You can use the streaming approach in such cases:

```xml
<resources>
    <string name="name1">content1</string>
    <string name="name2">content2</string>
    <string name="name3">content3</string>
</resources>
```

Can be parsed by:

```kotlin
data class Resource(val name: String, val content: String) {
  companion object {
    fun xml(k: Konsumer): Resource {
      k.checkCurrent("string")
      return Resource(k.attributes["name"], k.text())
    }
  }
}

File("parent.xml").konsumeXml().use {
    val konsumer = it.nextElement(Names.of("resources"), true)!!
    val resources: Sequence<Resource> = konsumer.childrenSequence("string") { Resource.xml(this) }
    resources.forEach { println(it) }
}
```

Note the use of the `use{}` function - it will make sure to close the `FileInputStream` properly.

You can rewrite the code in a different way, to return the `Sequence<Resource>` to the caller, closing the konsumer after the last item has been read.
However, this approach is very fragile: if an exception is thrown, the sequence might not be read to the very end and the
input stream might not have been closed properly. Use this approach with extreme caution.

## Skipping Elements

You can use the `skipContents()` function to skip the contents of the current element. For example, getting just the
name and keys from the property list is easy:

```xml
<propertyList>
   <name>example</name>
   <entry key="one">
      <value>first value</value>
   </entry>
   <entry key="two">
      <value>second value</value>
   </entry>
   <entry key="three">
      <value>third value</value>
   </entry>
</propertyList>
```

```kotlin
data class PropertyKeys(val name: String, val keys: List<String>) {
    companion object {
        fun xml(k: Konsumer): PropertyKeys {
            k.checkCurrent("propertyList")
            return PropertyKeys(k.childText("name"), k.children("entry") {
                val key = attributes["key"]
                skipContents()
                key
            })
        }
    }
}
```

To get just the name of the property list you can use the following code:

```kotlin
val name = xml.konsumeXml().child("propertyList") {
    val name = childText("name")
    skipContents()
    name
}
```

Of course this effectively disables validation within the `<propertyList>` element since the function will effectively
skip any contents it encounters. Therefore, if you wish to use the validation, you should still parse the XML contents and
throw away the results:

```kotlin
data class PropertyKeys(val name: String, val keys: List<String>) {
    companion object {
        fun validatingXml(k: Konsumer): PropertyKeys {
            k.checkCurrent("propertyList")
            return PropertyKeys(k.childText("name"), k.children("entry") {
                val key = attributes["key"]
                k.childText("value")
                key
            })
        }
    }
}
```

### Skipping Unknown Elements

Sometimes the XML may not adhere to any particular scheme, and in addition to useful elements it may contain unwanted
elements that we want to skip. If the order of children is known and you know the names of those unwanted elements,
then you can write something like the following:

```xml
<article>
  <title>Something</title>
  <description>Really cool article</description>
  <lastUpdateDate>Something we don't need for now</lastUpdateDate>
  <author>John Doe</author>
</article>
``` 

```kotlin
val xml = """the xml from above"""
xml.konsumeXml().child("article") {
    val title = childText("title")
    val desc = childText("description")
    child("lastUpdateDate") { skipContents() } // ignore <lastUpdateDate>
    // or if we know that the element will only contain text, you can read the text and throw it away:
    // childText("lastUpdateDate")
    val author = childText("author")
}
```

However, often the order of elements is unknown or may be random; also there could be any number of additional random
elements (possibly from other namespaces) that we need to ignore. There is the `children()` function which runs given
block for the contents of every child element found. We can use the function as follows:

```kotlin
xml.konsumeXml().child("article") {
    children(anyName) {
        when (localName) {
            "title" -> println(text())
            "description" -> println(text())
            else -> skipContents() // skip all unknown elements
        }
    }
}
```

There is even a convenience function `allChildrenAutoIgnore()` which automatically skips unknown elements:

```kotlin
xml.konsumeXml().child("article") {
    allChildrenAutoIgnore(Names.of("title", "description")) {
        when (localName) {
            "title" -> println(text())
            "description" -> println(text())
        }
    }
}
``` 

WARNING: the abovementioned functions will consume all child elements of the current element; on top of that
the `allChildrenAutoIgnore()`
will silently throw away all elements that are not listed as parameters of the function.

## Error Reporting

When a parse/conversion error happens, it is vital to properly report the error location (file name, row column) so that
the problematic spot in the XML can be discovered quickly.

All methods of Konsume-XML such as `child()`, `childText()` and others throw the `KonsumerException`
exception which contains the accurate location where the exception occurred. Moreover, `childText {}` conversion block
catches any exception thrown and automatically wraps it in `KonsumerException`.

It is a common case to perform more refined validation of the value outside of `childText()` function, in the parent
`child {}` block. For example one could validate that the user with given user name exists, or that a combination of
values from multiple elements makes sense (e.g. the total count of characters from `<firstname>` and `<surname>` is at least 5). 
Therefore, all `child {}`/`children {}`/... blocks also wrap any exceptions automatically in `KonsumerException`,
adding location info.

## Writing XMLs/Producing XMLs/Saving XMLs

That is unfortunately out-of-scope for this library - this library is intended to only read
the XMLs. However, there is an amazing simple library called [Kotlin XML Builder](https://github.com/redundent/kotlin-xml-builder)
which can be used for writing purposes.

I know, using two separate libraries requires writing the XML-related code two times:
once for parsing, and once for XML writing. Believe me, it still beats fighting with Jackson-XML
and figuring out a magic combination of annotations and parameters to make that
pile of jars produce something usable.

# Developing

Please feel free to open bug reports to discuss new features; PRs are welcome as well :)

## Using local build of konsume-xml

1. Clone this repository via `git clone https://gitlab.com/mvysny/konsume-xml` and build konsume-xml with `./gradlew publishToMavenLocal`. This will deploy the most current version into your local machine. See the `version` variable in [build.gradle.kts](build.gradle.kts)
   for the most up-to-date version; say it's 0.13-SNAPSHOT.
2. Add `repositories { mavenLocal() }` to your (Android) project.
3. Now add/modify the dependency on konsume-xml as follows: `dependencies { implementation("com.gitlab.mvysny.konsume-xml:konsume-xml:0.13-SNAPSHOT") }`

Done - your project will now use the locally-built konsume-xml.

## Releasing

To release the library to Maven Central:

1. Edit `build.gradle.kts` and remove `-SNAPSHOT` in the `version=` stanza
2. Commit with the commit message of simply being the version being released, e.g. "1.2.13"
3. git tag the commit with the same tag name as the commit message above, e.g. `1.2.13`
4. `git push`, `git push --tags`
5. Run `./gradlew clean build publish`
6. Continue to the [OSSRH Nexus](https://oss.sonatype.org/#stagingRepositories) and follow the [release procedure](https://central.sonatype.org/pages/releasing-the-deployment.html).
7. Add the `-SNAPSHOT` back to the `version=` while increasing the version to something which will be released in the future,
   e.g. 1.2.14, then commit with the commit message "1.2.14-SNAPSHOT" and push.

# FAQ

Q: I'm getting `XmlPullParserException: caused by: i.a.a.a: resource not found: /META-INF/services/org.xmlpull.v1.XmlPullParserFactory make sure that parser implementing XmlPull API is available`.

A: Your project is using the `xpp3:xpp3:1.1.3.3` library which doesn't work with Android properly. See Issue #17 for more details.
Please exclude the library from your project since Android SDK provides the very same API natively.

# License

See [LICENSE](LICENSE)

